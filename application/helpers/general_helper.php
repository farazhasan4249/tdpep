<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('single_image_upload')){	
	function single_image_upload($image,$path){         
		$_FILES['image']['name']= $image['name'];
		$_FILES['image']['type']= $image['type'];
		$_FILES['image']['tmp_name']= $image['tmp_name'];
		$_FILES['image']['error']= $image['error'];
		$_FILES['image']['size']= $image['size']; 
    if(!file_exists($path)){
      mkdir($path, 0777, true);
    }
    $ci =& get_instance();
    $config['upload_path'] = ''.$path.'';
    $config['allowed_types'] = 'gif|jpg|png';
    $ci->load->library('upload', $config);
    $ci->upload->initialize($config);
    if(!$ci->upload->do_upload('image')){ 
      return array('error' => $ci->upload->display_errors());
    }else{
      $file_detail = $ci->upload->data();				
			return	$file_detail['file_name'];			
    }
    return FALSE;
	}
}

// if (!function_exists('pagenation'))
// {
//   function pagenation($page_url="",$per_page="",$total_row="")
//    { 
//     $ci =& get_instance(); 
//     $data["base_url"] = $page_url;
//     $data["total_rows"] = $total_row;
//     $data["per_page"] = $per_page;
//     $data['use_page_numbers'] = FALSE;

//     $data['first_link'] = '';
//     $data['last_link'] = ''; 

//     $data['next_tag_open'] = '<li class="tg-prevpage"><a href="javascript:">';
//     $data['next_tag_close'] = '</a></li>';

//     $data['prev_tag_open'] = '<li class="tg-prevpage"><a href="javascript:">';
//     $data['prev_tag_close'] = '</a></li>';

//     $data['num_tag_open'] = '<li><a>';
//     $data['num_tag_close'] = '</a></li>';

//     $data['cur_tag_open'] = '<li class="tg-active"><a>';
//     $data['cur_tag_close'] = '</a></li>';
    
//     $ci->pagination->initialize($data);
//     $links = $ci->pagination->create_links();
//     return $links;          
//    }
// }


if(!function_exists('multi_image_upload')){	
	function multi_image_upload($image,$index,$path){         
		$_FILES['image']['name']= $image['name'][$index];
		$_FILES['image']['type']= $image['type'][$index];
		$_FILES['image']['tmp_name']= $image['tmp_name'][$index];
		$_FILES['image']['error']= $image['error'][$index];
		$_FILES['image']['size']= $image['size'][$index];
    if(!file_exists($path)){
      mkdir($path, 0777, true);
    }
    $ci =& get_instance();
    $config['upload_path'] = ''.$path.'';
    $config['allowed_types'] = 'gif|jpg|png';
    $ci->load->library('upload', $config);
    $ci->upload->initialize($config);
    if(!$ci->upload->do_upload('image')){ 
      return array('error' => $ci->upload->display_errors());
    }else{
      $file_detail = $ci->upload->data();				
			return	$file_detail['file_name'];			
    }
    return FALSE;
	}
}

if (!function_exists('get_id_by_slug'))
{
  function get_id_by_slug($table,$slug)
  {               
    $ci =& get_instance();
    $temp = $ci->general->get_list($table,array(''.$table.'_slug'=>$slug));
    if($temp){      
      foreach($temp as $tp){
          $id = $table.'_id';
        $result = $tp->$id;       
      }
      return $result;
    }else{
      return FALSE;
    } 
  }
} 

if (!function_exists('get_slug_by_id'))
{
  function get_slug_by_id($table,$id)
  {               
    $ci =& get_instance();
    $temp = $ci->general->get_list($table,$table.'_id='.$id);
    if($temp){      
      $slug = $table.'_slug';
      foreach($temp as $tp){
        $result = $tp->$slug;       
      }
      return $result;
    }else{
      return FALSE;
    } 
  }
} 

if (!function_exists('get_name_by_id'))
{
  function get_name_by_id($table,$id)
  {               
    $ci =& get_instance();
    $temp = $ci->general->get_list($table,$table.'_id='.$id);
    if($temp){      
      $name = $table.'_name';
      foreach($temp as $tp){
        $result = $tp->$name;       
      }
      return $result;
    }else{
      return FALSE;
    } 
  }
}

if (!function_exists('get_cat_count'))
{
  function get_cat_count($id)
  { 
    $ci =& get_instance();
    $result = $ci->general->get_list("product",array("category_id"=>$id));
    return count($result);
    
  }
}
if (!function_exists('get_subcat_count'))
{
  function get_subcat_count($id)
  { 
    $ci =& get_instance();
    $result = $ci->general->get_list("product",array("sub_category_id"=>$id));
    return count($result);
    
  }
}

if (!function_exists('get_product_count'))
{
  function get_product_count($id)
  { 
    $ci =& get_instance();
    $result = $ci->general->get_list("product",array("product_id"=>$id));
    return count($result);
    
  }
}


 function upload_file($table,$field)
  {
    $config['upload_path'] = './uploads/'.$table.'';
    $config['allowed_types'] = '*';
    $config['max_size']     = '1000000000000000';
    $config['max_width'] = '40000';
    $config['max_height'] = '20000';
    $this->load->library('upload', $config);
    $this->upload->initialize($config); 
      
    if($this->upload->do_upload($field)){ 

      $file_detail = $this->upload->data(); 
      return $file_detail['file_name'];
          
    }else{
      echo $this->upload->display_errors(); 
      
                
    }     
    return;
  }

  if(!function_exists('single_video_upload')){  
  function single_video_upload($video,$path){         
    $_FILES['video']['name']= $video['name'];
    $_FILES['video']['type']= $video['type'];
    $_FILES['video']['tmp_name']= $video['tmp_name'];
    $_FILES['video']['error']= $video['error'];
    $_FILES['video']['size']= $video['size']; 
    if(!file_exists($path)){
      mkdir($path, 0777, true);
    }
    $ci =& get_instance();
    $config['upload_path'] = ''.$path.'';
    $config['allowed_types'] = 'mp4|mkv|flv';
    $ci->load->library('upload', $config);
    $ci->upload->initialize($config);
    if(!$ci->upload->do_upload('video')){ 
      return array('error' => $ci->upload->display_errors());
    }else{
      $file_detail = $ci->upload->data();       
      return  $file_detail['file_name'];      
    }
    return FALSE;
  }
}

function upload_video($table,$field)
  {
    $config['upload_path'] = './uploads/'.$table.'';
    $config['allowed_types'] = '*';
    $config['max_size']     = '100000000000000000';
    $this->load->library('upload', $config);
    $this->upload->initialize($config); 
      
    if($this->upload->do_upload($field)){ 

      $file_detail = $this->upload->data(); 
      return $file_detail['file_name'];
          
    }else{
      echo $this->upload->display_errors(); 
      
                
    }     
    return;
  }

if(!function_exists('send_email')){	
	function send_email($send_to,$subject,$body){
    $ci =& get_instance();
    $config['mailtype'] ='html';
		$ci->email->initialize($config);    
		$ci->email->from($ci->email_from,$ci->site_title);
		$ci->email->to($send_to);		
		$ci->email->subject($subject);
		$ci->email->message($body);
		if($ci->email->send()){
			return TRUE;	
		}else{
			return FALSE;
		}
  }
}

if (!function_exists('get_list'))
{
  function get_list($tabel="",$where="",$limit="",$order_col="",$order_by="",$like="")
  {   
    $ci =& get_instance();
    $records = $ci->general->get_list($tabel,$where,$limit,$order_col,$order_by,$like);
    if($records){ 
      return $records;
    }else{
      return  FALSE;
    }
      
  }
}

if (!function_exists('limit_text'))
{
 function limit_text($count,$text)
 {
  $result = strlen($text)>$count?substr($text, 0, $count).'...':$text;
    return $result;
 } 
}


