<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    function __construct() {
    parent::__construct();      
    }  

     public function list(){ 
        $data['table'] = 'contact';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Contact Inquiry';
        $content['main_content'] = 'contact/list';          
        $this->load->view('admin/inc/view',$content);
}

public function delete($id)
    {
         $content = array(
            'contact_status' => 'disable',
            'contact_updated_by' => '1',
        );
        $data['where'] = array('contact_id' => $id);
        $data['table'] = 'contact';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/contact/list');
    }
    
  public function view($id)
    {
        $data['where'] = array('contact_id' => $id);
        $data['table'] = 'contact';
        $data['output_type'] = 'row';
        $content['title'] = 'Contact Info';
        $content['record'] = $this->general->get($data);
        $content['main_content'] = 'contact/view';
        $this->load->view('admin/inc/view', $content);
    }

}
