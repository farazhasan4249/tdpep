<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function index()
    {   
        $data['table'] = 'product';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Products';
        $content['main_content'] = 'products/list';          
        $this->load->view('admin/inc/view',$content);
    }

    public function add()
    {

        $data = array();
        $data['table']='product';

        $data = array();
        $data['table'] = 'category';
        $data['join_table'] = 'sub_category';
        $data['join'] = 'category.category_id = sub_category.category_id';
        $data['join_type'] = 'left';
        $content['category']=$this->general->get($data);
        $content['sub_category'] = $this->general->get($data);




        if($_POST)
        {
            $this->form_validation->set_rules('product_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
             $this->form_validation->set_rules('product_slug', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_reg_price', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_discounted_price', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_short_desc', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_long_desc', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            if(!$this->form_validation->run() == FALSE)
            {
                // print_r($content);exit;

                $content = array(
                    'category_id' => $this->input->post('category_id',TRUE),
                    'sub_category_id' => $this->input->post('sub_category_id',TRUE),

                    'product_name' => $this->input->post('product_name', TRUE),
                     'product_slug' => $this->input->post('product_slug',TRUE),
                    // 'product_featured' => $this->input->post('product_featured',TRUE),
                    'product_image' => $this->input->post('product_image',TRUE),
                    'product_reg_price' => $this->input->post('product_reg_price',TRUE),
                    'product_discounted_price' => $this->input->post('product_discounted_price',TRUE),
                    'product_short_desc' => $this->input->post('product_short_desc',TRUE),
                    'product_long_desc' => $this->input->post('product_long_desc',TRUE),
                    'product_status' => 'enable',
                    'product_created_by' => '1'
                );

                if($_FILES['product_image']['size'] > 0){
                    $image = single_image_upload($_FILES['product_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['product_image'] = $image;
                    }
                } 
                
                $data['table'] = 'product';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/products');
            }
            else
            {   
                $content['main_content'] = 'products/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'product';
            $content['main_content'] = 'products/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }
    
    public function edit($id)
    {

        $data = array();
        $data['table']='category';
        $content['categories']=$this->general->get($data);

        $data = array();
        $data['table']='sub_category';
        $content['sub_category']=$this->general->get($data);


        if($_POST)
        {
            $this->form_validation->set_rules('product_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_slug', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_reg_price', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_discounted_price', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_short_desc', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('product_long_desc', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
    
           
            if(!$this->form_validation->run() == FALSE)
            {
                $content = array(

                   'category_id' => $this->input->post('category_id',TRUE),
                   'sub_category_id' => $this->input->post('sub_category_id',TRUE), 
                   'product_name' => $this->input->post('product_name', TRUE),
                    'product_slug' => $this->input->post('product_slug',TRUE),
                    //'product_featured' => $this->input->post('product_featured',TRUE),
                    'product_image' => $this->input->post('product_image',TRUE),
                    'product_reg_price' => $this->input->post('product_reg_price',TRUE),
                    'product_discounted_price' => $this->input->post('product_discounted_price',TRUE),
                    'product_short_desc' => $this->input->post('product_short_desc',TRUE),
                    'product_long_desc' => $this->input->post('product_long_desc',TRUE),
                    'product_status' => 'enable',
                    'product_created_by' => '1'
                );
                if($_FILES['product_image']['size'] > 0){
                    $image = single_image_upload($_FILES['product_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['product_image'] = $image;
                    }
                } 
                    $data['where'] = array('product_id'=>$id);
                    $data['table'] = 'product';
                    $insert_id = $this->general->update($data, $content);

                    $this->session->set_flashdata('success', 'Updated Successfully.');
                    redirect('admin/products');
            }
            else
            {

                $data['where'] = array('product_id'=>$id);
                $data['table'] = 'product';
                $data['output_type'] = 'row';
                $content['record'] = $this->general->get($data);
                
                $content['title'] = 'products';
                $content['main_content'] = 'products/edit';          
                $this->load->view('admin/inc/view',$content);
            }
        }

        else
        {
            $data['where'] = array('product_id'=>$id);
            $data['table'] = 'product';
            $data['output_type'] = 'row';
            $content['record'] = $this->general->get($data);
            $content['title'] = 'product';
            $content['main_content'] = 'products/edit';          
            $this->load->view('admin/inc/view',$content);
        }  
    }
    public function view($id)
    {   
        $data['where'] = array('product_id'=>$id);
        $data['table'] = 'product';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'product';
        $content['main_content'] = 'products/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'product_status' => 'disable',
            'product_updated_by' => '1',
        );
        $data['where'] = array('product_id' => $id);
        $data['table'] = 'product';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/products');
        
    }

    public function get_dropdown()
    {       
        $id = $this->input->post("id");
        $get_from = $this->input->post("get_from");
        $get_where = $this->input->post("get_where");
        $get_from_id = $get_from.'_id';
        $options = $this->general->get_list($get_from,array($get_where.'_id'=>$id));
        echo '<option value="">Please Select</option>';
        foreach($options as $option){
            echo '<option value="'. $option->$get_from_id .'">'. get_name_by_id($get_from,$option->$get_from_id) .'</option>';
        }
        return; 
    }
        
    }

?>