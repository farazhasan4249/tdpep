<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
    function __construct() {
    parent::__construct();		
    }  

     public function list(){ 
        $data['table'] = 'newsletter';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Newsletter';
        $content['main_content'] = 'newsletter/list';          
        $this->load->view('admin/inc/view',$content);
}

public function delete($id)
    {
         $content = array(
            'newsletter_status' => 'disable',
            'newsletter_updated_by' => '1',
        );
        $data['where'] = array('newsletter_id' => $id);
        $data['table'] = 'newsletter';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/newsletter/list');
    }

}
