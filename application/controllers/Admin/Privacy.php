<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function edit()
    {   
      if($_POST){
       
            $this->form_validation->set_rules('privacy_heading', 'privacy heading', 'required');
            $this->form_validation->set_rules('privacy_subheading', 'privacy Sub heading', 'required');

            $this->form_validation->set_rules('privacy_content', 'privacy Content', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'privacy_heading' => $this->input->post('privacy_heading',TRUE),
                'privacy_subheading' => $this->input->post('privacy_subheading',TRUE),
                'privacy_content' => $this->input->post('privacy_content',TRUE),
                'privacy_status' => 'enable',
                'privacy_updated_by' => '1',
                );    
                

                $data['where'] = array('privacy_id' => 1);     
                $data['table'] = 'privacy';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/privacy/edit');
            }else{        
                $data['where'] = array('privacy_id' => 1);     
                $data['table'] = 'privacy';    
                $data['output_type'] = 'row';   
                $content['title'] = 'privacy'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'privacy/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('privacy_id' => 1);     
            $data['table'] = 'privacy';    
            $data['output_type'] = 'row';   
            $content['title'] = 'Privacy'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'privacy/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>