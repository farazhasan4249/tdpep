<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {
    function __construct() {
    parent::__construct();	

    $this->load->model('general', 'g');	
    }  

     public function index()
    {   
        $data['table'] = 'home';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Home';
        $content['main_content'] = 'home/list';          
        $this->load->view('admin/inc/view',$content);
    }

      public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('home_slider_heading', 'Slider Heading', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('home_slider_text', 'Slider Text', 'trim|required|min_length[3]|max_length[300]');
            
            
            
            if(!$this->form_validation->run() == FALSE)
            {

                $content = array(
                    'home_slider_heading' => $this->input->post('home_slider_heading', TRUE),
                    'home_slider_text' => $this->input->post('home_slider_text',TRUE),
                    'home_slider_image' => $this->input->post('home_slider_image',TRUE),
                    'home_status' => 'enable',
                    'home_created_by' => '1'
                );
                if($_FILES['home_slider_image']['size'] > 0){
                    $image = single_image_upload($_FILES['home_slider_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['home_slider_image'] = $image;
                    }
                } 
                
                $data['table'] = 'home';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/home');
            }
            else
            {   
                $content['main_content'] = 'home/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'home';
            $content['main_content'] = 'home/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }

    public function edit()
    {   
      if($_POST){
     // print_r($_POST); exit; 
            $this->form_validation->set_rules('home_slider_heading', 'Home Sec heading', 'required');
            $this->form_validation->set_rules('home_slider_text', 'Home Sec Content heading', 'required');
           
            //$this->form_validation->set_rules('service_page_heading', 'Service Page Heading', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'home_slider_heading' => $this->input->post('home_slider_heading',TRUE),
                'home_slider_text' => $this->input->post('home_slider_text',TRUE),
                'home_slider_image' => $this->input->post('home_slider_image',TRUE),
              //  'service_page_heading' => $this->input->post('service_page_heading',TRUE),
                'home_status' => 'enable',
                'home_updated_by' => '1',
                );

                if($_FILES['home_slider_image']['size'] > 0){
                    $video = single_video_upload($_FILES['home_slider_image'],'./uploads/videos');
                    if(is_array($video)){            
                        $this->session->set_flashdata('error', $video);
                    }else{
                        $content['home_slider_image'] = $video;
                    }
                }    
                

                $data['where'] = array('home_id' => 1);     
                $data['table'] = 'home';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/home/edit');
            }else{        
                $data['where'] = array('home_id' => 1);     
                $data['table'] = 'home';    
                $data['output_type'] = 'row';   
                $content['title'] = 'Home'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'home/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('home_id' => 1);     
            $data['table'] = 'home';    
            $data['output_type'] = 'row';   
            $content['title'] = 'Home'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'home/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }

     public function view($id)
    {   
        $data['where'] = array('home_id'=>$id);
        $data['table'] = 'home';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'home';
        $content['main_content'] = 'home/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'home_status' => 'disable',
            'home_updated_by' => '1',
        );
        $data['where'] = array('home_id' => $id);
        $data['table'] = 'home';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/home');
        
    }
}
?>