<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function index()
    {   
        $data['table'] = 'sub_category';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Subcategory';
        $content['main_content'] = 'subcategory/list';          
        $this->load->view('admin/inc/view',$content);
    }

    public function add()
    {

        $data = array();
        $data['table']='category';
        $content['category']=$this->general->get($data);
        
        if($_POST)
        {
            $this->form_validation->set_rules('sub_category_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('sub_category_text', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            
            
            
            if(!$this->form_validation->run() == FALSE)
            {

                $content = array(
                    'category_id' => $this->input->post('category_id',TRUE),
                    'sub_category_name' => $this->input->post('sub_category_name', TRUE),
                    'sub_category_image' => $this->input->post('sub_category_image',TRUE),
                    'sub_category_text' => $this->input->post('sub_category_text',TRUE),
                    'sub_category_slug' => $this->input->post('sub_category_slug',TRUE),
                    'sub_category_status' => 'enable',
                    'sub_category_created_by' => '1'
                );
                if($_FILES['sub_category_image']['size'] > 0){
                    $image = single_image_upload($_FILES['sub_category_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['sub_category_image'] = $image;
                    }
                } 
                
                $data['table'] = 'sub_category';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/subcategory');
            }
            else
            {   
                $content['main_content'] = 'subcategory/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'subcategory';
            $content['main_content'] = 'subcategory/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }
    
    public function edit($id)
    {
        if($_POST)
        {

            $this->form_validation->set_rules('sub_category_name', 'Sub Category Name', 'trim|required|min_length[3]|max_length[300]');
            
    
           
            if(!$this->form_validation->run() == FALSE)
            {
                $content = array(
                    
                    'sub_category_name' => $this->input->post('sub_category_name', TRUE),
                    'sub_category_slug' => $this->input->post('sub_category_slug'.TRUE),
                    'sub_category_image' => $this->input->post('sub_category_image',TRUE),
                    'sub_category_status' => 'enable',
                    'sub_category_created_by' => '1'
                );
                if($_FILES['sub_category_image']['size'] > 0){
                    $image = single_image_upload($_FILES['sub_category_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['sub_category_image'] = $image;
                    }
                } 
                    $data['where'] = array('sub_category_id'=>$id);
                    $data['table'] = 'sub_category';
                    $insert_id = $this->general->update($data, $content);

                    $this->session->set_flashdata('success', 'Updated Successfully.');
                    redirect('admin/subcategory');
            }
            else
            {

                $data['where'] = array('sub_category_id'=>$id);
                $data['table'] = 'sub_category';
                $data['output_type'] = 'row';
                $content['record'] = $this->general->get($data);
                
                $content['title'] = 'Sub category';
                $content['main_content'] = 'subcategory/edit';          
                $this->load->view('admin/inc/view',$content);
            }
        }

        else
        {
            $data['where'] = array('sub_category_id'=>$id);
            $data['table'] = 'sub_category';
            $data['output_type'] = 'row';
            $content['record'] = $this->general->get($data);
            $content['title'] = 'Sub category';
            $content['main_content'] = 'subcategory/edit';          
            $this->load->view('admin/inc/view',$content);
        }  
    }
    public function view($id)
    {   
        $data['where'] = array('sub_category_id'=>$id);
        $data['table'] = 'sub_category';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'sub_category';
        $content['main_content'] = 'subcategory/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'sub_category_status' => 'disable',
            'sub_category_updated_by' => '1',
        );
        $data['where'] = array('sub_category_id' => $id);
        $data['table'] = 'sub_category';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/subcategory');
        
    }
        
    }

?>