<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mission extends Admin_Controller {
    function __construct() {
    parent::__construct();		
    }  

    public function index(){ 
        if($_POST){
            $this->form_validation->set_rules('mission_text', 'Mission Text', 'required');
            $this->form_validation->set_rules('mission_heading', 'Mission Heading', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'mission_text' => $this->input->post('mission_text',TRUE),
                'mission_img' => $this->input->post('mission_img',TRUE),
                'mission_heading' => $this->input->post('mission_heading',TRUE),
                'mission_status' => 'enable',
                'mission_updated_by' => '1',

                );    
                if($_FILES['mission_img']['size'] > 0){
                    $image = single_image_upload($_FILES['mission_img'],'./uploads/cms');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['mission_img'] = $image;
                    }
                } 

                $data['where'] = array('mission_id' => 1);		
                $data['table'] = 'mission';	
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/mission');
            }else{        
                $data['where'] = array('mission_id' => 1);		
                $data['table'] = 'mission';	
                $data['output_type'] = 'row';	
                $content['title'] = 'Mission';	
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'mission/edit';			
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('mission_id' => 1);		
            $data['table'] = 'mission';	
            $data['output_type'] = 'row';	
            $content['title'] = 'Mission';	
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'mission/edit';			
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
