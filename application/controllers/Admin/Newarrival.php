<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newarrival extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function index()
    {   
        $data['table'] = 'newarrival';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'New Arrival';
        $content['main_content'] = 'newarrival/list';          
        $this->load->view('admin/inc/view',$content);
    }

    public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('newarrival_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            
            
            
            if(!$this->form_validation->run() == FALSE)
            {

                $content = array(
                    'newarrival_name' => $this->input->post('newarrival_name', TRUE),
                    'newarrival_image' => $this->input->post('newarrival_image',TRUE),
                    'newarrival_status' => 'enable',
                    'newarrival_created_by' => '1'
                );
                if($_FILES['newarrival_image']['size'] > 0){
                    $image = single_image_upload($_FILES['newarrival_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['newarrival_image'] = $image;
                    }
                } 
                
                $data['table'] = 'newarrival';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/newarrival');
            }
            else
            {   
                $content['main_content'] = 'newarrival/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'newarrival';
            $content['main_content'] = 'newarrival/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }
    
    public function edit($id)
    {
        if($_POST)
        {

            $this->form_validation->set_rules('newarrival_name', 'Newarrival Name', 'trim|required|min_length[3]|max_length[300]');
            
    
           
            if(!$this->form_validation->run() == FALSE)
            {
                $content = array(
                    
                    'newarrival_name' => $this->input->post('newarrival_name', TRUE),
                    'newarrival_image' => $this->input->post('newarrival_image',TRUE),
                    'newarrival_status' => 'enable',
                    'newarrival_created_by' => '1'
                );
                if($_FILES['newarrival_image']['size'] > 0){
                    $image = single_image_upload($_FILES['newarrival_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['newarrival_image'] = $image;
                    }
                } 
                    $data['where'] = array('newarrival_id'=>$id);
                    $data['table'] = 'newarrival';
                    $insert_id = $this->general->update($data, $content);

                    $this->session->set_flashdata('success', 'Updated Successfully.');
                    redirect('admin/newarrival');
            }
            else
            {

                $data['where'] = array('newarrival_id'=>$id);
                $data['table'] = 'newarrival';
                $data['output_type'] = 'row';
                $content['record'] = $this->general->get($data);
                
                $content['title'] = 'newarrival';
                $content['main_content'] = 'newarrival/edit';          
                $this->load->view('admin/inc/view',$content);
            }
        }

        else
        {
            $data['where'] = array('newarrival_id'=>$id);
            $data['table'] = 'newarrival';
            $data['output_type'] = 'row';
            $content['record'] = $this->general->get($data);
            $content['title'] = 'newarrival';
            $content['main_content'] = 'newarrival/edit';          
            $this->load->view('admin/inc/view',$content);
        }  
    }
    public function view($id)
    {   
        $data['where'] = array('newarrival_id'=>$id);
        $data['table'] = 'newarrival';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'newarrival';
        $content['main_content'] = 'newarrival/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'newarrival_status' => 'disable',
            'newarrival_updated_by' => '1',
        );
        $data['where'] = array('newarrival_id' => $id);
        $data['table'] = 'newarrival';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/newarrival');
        
    }
        
    }

?>