<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends Admin_Controller {
    function __construct() {
    parent::__construct();		
    }  

    public function index(){ 	
        $data['table'] = 'service';	
        $data['output_type'] = 'result';	
        $content['title'] = 'Services';			
        $content['records']  = $this->general->get($data);
        $content['main_content'] = 'service/list';			
        $this->load->view('admin/inc/view',$content);   
    }
      
    public function edit($id){ 
        if($_POST){  
            $this->form_validation->set_rules('service_heading', 'Heading', 'trim|required|min_length[3]|max_length[300]');
            $this->form_validation->set_rules('service_text', 'Text', 'trim|required');
            if (!$this->form_validation->run() == FALSE){
                $content = array(
                'service_heading' => $this->input->post('service_heading',TRUE),
                'service_text' => $this->input->post('service_text',TRUE),
                'service_icon' => $this->input->post('service_icon',TRUE),
                'service_img' => $this->input->post('service_img',TRUE),
                'service_status' => 'enable',
                'service_updated_by' => '1'
                );   
                if($_FILES['service_icon']['size'] > 0){
                    $image = single_image_upload($_FILES['service_icon'],'./uploads/cms');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['service_icon'] = $image;
                    }
                } 
                if($_FILES['service_img']['size'] > 0){
                    $image = single_image_upload($_FILES['service_img'],'./uploads/cms');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['service_img'] = $image;
                    }
                }  
                $data['where'] = array('service_id' => $id);	
                $data['table'] = 'service';	
                $this->general->update($data,$content);        
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/service');
            }else{        
                $data['where'] = array('service_id' => $id);		
                $data['table'] = 'service';	
                $data['output_type'] = 'row';	
                $content['title'] = 'Services';	
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'service/edit';			
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        		
            $data['where'] = array('service_id' => $id);	
            $data['table'] = 'service';	
            $data['output_type'] = 'row';	
            $content['title'] = 'Services';	
            $content['record']  = $this->general->get($data);
            $content['main_content'] = 'service/edit';			
            $this->load->view('admin/inc/view',$content); 
        } 
    }
    public function view($id)
    {
        $data['where'] = array('service_id' => $id);
        $data['table'] = 'service';
        $data['output_type'] = 'row';
        $content['title'] = 'Services';
        $content['record'] = $this->general->get($data);
        $content['main_content'] = 'service/view';
        $this->load->view('admin/inc/view',$content);
    }
}

