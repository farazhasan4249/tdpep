<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thirdsec extends Admin_Controller {
    function __construct() {
    parent::__construct();	

    $this->load->model('general', 'g');	
    }  

    public function edit()
    {   
      if($_POST){

            $this->form_validation->set_rules('third_section_heading', 'Third Sec heading', 'required');
            $this->form_validation->set_rules('third_section_button_text', 'Third Sec button text', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'third_section_heading' => $this->input->post('third_section_heading',TRUE),
                'third_section_butn_text' => $this->input->post('third_section_butn_text',TRUE),
                'third_section_video' => $this->input->post('third_section_video',TRUE),
                'third_section_status' => 'enable',
                'third_section_updated_by' => '1',
                );

                if($_FILES['third_section_video']['size'] > 0){
                    $video = single_video_upload($_FILES['third_section_video'],'./uploads/videos');
                    if(is_array($video)){            
                        $this->session->set_flashdata('error', $video);
                    }else{
                        $content['third_section_video'] = $video;
                    }
                }    
                

                $data['where'] = array('third_section_id' => 1);     
                $data['table'] = 'third_section';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/Thirdsection/edit');
            }else{        
                $data['where'] = array('third_section_id' => 1);     
                $data['table'] = 'third_section';    
                $data['output_type'] = 'row';   
                $content['title'] = 'third section'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'thirdsection/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('third_section_id' => 1);     
            $data['table'] = 'third_section';    
            $data['output_type'] = 'row';   
            $content['title'] = 'third section'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'thirdsection/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
      }
    }
  ?>