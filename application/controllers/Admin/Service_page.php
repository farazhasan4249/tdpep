<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_page extends Admin_Controller {
    function __construct() {
    parent::__construct();		
    }  

    public function index(){ 
        if($_POST){
            $this->form_validation->set_rules('service_page_txt', 'Service Page Text', 'required');
            $this->form_validation->set_rules('service_page_heading', 'Service Page Heading', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'service_page_txt' => $this->input->post('service_page_txt',TRUE),
                'service_page_img' => $this->input->post('service_page_img',TRUE),
                'service_page_heading' => $this->input->post('service_page_heading',TRUE),
                'service_page_status' => 'enable',
                'service_page_updated_by' => '1',

                );    
                if($_FILES['service_page_img']['size'] > 0){
                    $image = single_image_upload($_FILES['service_page_img'],'./uploads/cms');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['service_page_img'] = $image;
                    }
                } 

                $data['where'] = array('service_page_id' => 1);		
                $data['table'] = 'service_page';	
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/service_page');
            }else{        
                $data['where'] = array('service_page_id' => 1);		
                $data['table'] = 'service_page';	
                $data['output_type'] = 'row';	
                $content['title'] = 'Service Page';	
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'service_page/edit';			
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('service_page_id' => 1);		
            $data['table'] = 'service_page';	
            $data['output_type'] = 'row';	
            $content['title'] = 'Service Page';	
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'service_page/edit';			
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
