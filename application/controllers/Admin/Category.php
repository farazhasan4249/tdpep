<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function index()
    {   
        $data['table'] = 'category';
        $data['output_type'] = 'result';
        $content['records'] = $this->general->get($data);
        $content['title'] = 'Category';
        $content['main_content'] = 'category/list';          
        $this->load->view('admin/inc/view',$content);
    }

    public function add()
    {
        if($_POST)
        {
            $this->form_validation->set_rules('category_name', 'Name', 'trim|required|min_length[3]|max_length[300]');
            
            
            
            
            if(!$this->form_validation->run() == FALSE)
            {

                $content = array(
                    'category_name' => $this->input->post('category_name', TRUE),
                    'category_slug' => $this->input->post('category_slug', TRUE),
                    'category_image' => $this->input->post('category_image',TRUE),
                    'category_status' => 'enable',
                    'category_created_by' => '1'
                );
                if($_FILES['category_image']['size'] > 0){
                    $image = single_image_upload($_FILES['category_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['category_image'] = $image;
                    }
                } 
                
                $data['table'] = 'category';
                $insert_id = $this->general->insert($data, $content);
                $this->session->set_flashdata('success', 'Added Successfully.');
                redirect('admin/category');
            }
            else
            {   
                $content['main_content'] = 'category/add';           
                $this->load->view('admin/inc/view',$content); 
            }
        }
        else
        {   
            $content['title'] = 'category';
            $content['main_content'] = 'category/add';           
            $this->load->view('admin/inc/view',$content);  
        }  
    }
    
    public function edit($id)
    {
        if($_POST)
        {

            $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required|min_length[3]|max_length[300]');
            
    
           
            if(!$this->form_validation->run() == FALSE)
            {
                $content = array(
                    
                    'category_name' => $this->input->post('category_name', TRUE),
                    'category_image' => $this->input->post('category_image',TRUE),
                    'category_slug' => $this->input->post('category_slug', TRUE),
                    'category_status' => 'enable',
                    'category_created_by' => '1'
                );
                if($_FILES['category_image']['size'] > 0){
                    $image = single_image_upload($_FILES['category_image'],'./uploads/settings');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['category_image'] = $image;
                    }
                } 
                    $data['where'] = array('category_id'=>$id);
                    $data['table'] = 'category';
                    $insert_id = $this->general->update($data, $content);

                    $this->session->set_flashdata('success', 'Updated Successfully.');
                    redirect('admin/category');
            }
            else
            {

                $data['where'] = array('category_id'=>$id);
                $data['table'] = 'category';
                $data['output_type'] = 'row';
                $content['record'] = $this->general->get($data);
                
                $content['title'] = 'category';
                $content['main_content'] = 'category/edit';          
                $this->load->view('admin/inc/view',$content);
            }
        }

        else
        {
            $data['where'] = array('category_id'=>$id);
            $data['table'] = 'category';
            $data['output_type'] = 'row';
            $content['record'] = $this->general->get($data);
            $content['title'] = 'category';
            $content['main_content'] = 'category/edit';          
            $this->load->view('admin/inc/view',$content);
        }  
    }
    public function view($id)
    {   
        $data['where'] = array('category_id'=>$id);
        $data['table'] = 'category';
        $data['output_type'] = 'row';
        $content['record'] = $this->general->get($data);
    
        $content['title'] = 'category';
        $content['main_content'] = 'category/view';          
        $this->load->view('admin/inc/view',$content);
      
    }
    public function delete($id)
    {
         $content = array(
            'category_status' => 'disable',
            'category_updated_by' => '1',
        );
        $data['where'] = array('category_id' => $id);
        $data['table'] = 'category';
        $this->general->update($data, $content);
        $this->session->set_flashdata('success', 'Delete Successfully.');
        redirect('admin/category');
        
    }
        
    }

?>