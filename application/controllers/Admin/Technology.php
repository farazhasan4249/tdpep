<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technology extends Admin_Controller {
    function __construct() {
    parent::__construct();	

    $this->load->model('general', 'g');	
    }  

    public function edit()
    {   
      if($_POST){

 $this->form_validation->set_rules('technology_section_heading', 'Technology Section heading', 'required');
 $this->form_validation->set_rules('technology_section_subheadingone', 'Technology Section Subheading One', 'required');
     $this->form_validation->set_rules('technology_section_contentone', 'Technology Section content One', 'required');
     $this->form_validation->set_rules('technology_section_subheadingtwo', 'Technology Section Subheading two', 'required');
     $this->form_validation->set_rules('technology_section_contenttwo', 'Technology Section content two', 'required');
     $this->form_validation->set_rules('technology_section_subheadingthree', 'Technology Section Subheading three', 'required');
     $this->form_validation->set_rules('technology_section_contentthree', 'Technology Section content three', 'required');
     $this->form_validation->set_rules('technology_section_subheadingfour', 'Technology Section subheading four', 'required');
     $this->form_validation->set_rules('technology_section_contentfour', 'Technology Section content four', 'required');
     $this->form_validation->set_rules('technology_section_subheadingfive', 'Technology Section subheading five', 'required');
     $this->form_validation->set_rules('technology_section_contentfive', 'Technology Section content five', 'required');
     $this->form_validation->set_rules('technology_section_subheadingsix', 'Technology Section subheading six', 'required');
     $this->form_validation->set_rules('technology_section_contentsix', 'Technology Section content six', 'required');
     $this->form_validation->set_rules('technology_section_subheadingseven', 'Technology Section subheading seven', 'required');
     $this->form_validation->set_rules('technology_section_contentseven', 'Technology Section content seven', 'required');
     $this->form_validation->set_rules('technology_section_subheadingeight', 'Technology Section subheading eight', 'required');
     $this->form_validation->set_rules('technology_section_contenteight', 'Technology Section content eight', 'required');
     $this->form_validation->set_rules('technology_section_subheadingnine', 'Technology Section subheading nine', 'required');
     $this->form_validation->set_rules('technology_section_contentnine', 'Technology Section content nine', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'technology_section_heading' => $this->input->post('technology_section_heading',TRUE),
                'technology_section_subheadingone' => $this->input->post('technology_section_subheadingone',TRUE),
                'technology_section_contentone' => $this->input->post('technology_section_contentone',TRUE),
                'technology_section_subheadingtwo' => $this->input->post('technology_section_subheadingtwo',TRUE),
                'technology_section_contenttwo' => $this->input->post('technology_section_contenttwo',TRUE),
                'technology_section_subheadingthree' => $this->input->post('technology_section_subheadingthree',TRUE),
                'technology_section_contentthree' => $this->input->post('technology_section_contentthree',TRUE),
                'technology_section_subheadingfour' => $this->input->post('technology_section_subheadingfour',TRUE),
                'technology_section_contentfour' => $this->input->post('technology_section_contentfour',TRUE),
                'technology_section_subheadingfive' => $this->input->post('technology_section_subheadingfive',TRUE),
                'technology_section_contentfive' => $this->input->post('technology_section_contentfive',TRUE),
                'technology_section_subheadingsix' => $this->input->post('technology_section_subheadingsix',TRUE),
                'technology_section_contentsix' => $this->input->post('technology_section_contentsix',TRUE),
                'technology_section_subheadingseven' => $this->input->post('technology_section_subheadingseven',TRUE),
                'technology_section_contentseven' => $this->input->post('technology_section_contentseven',TRUE),
                'technology_section_subheadingeight' => $this->input->post('technology_section_subheadingeight',TRUE),
                'technology_section_contenteight' => $this->input->post('technology_section_contenteight',TRUE),
                'technology_section_subheadingnine' => $this->input->post('technology_section_subheadingnine',TRUE),
                'technology_section_contentnine' => $this->input->post('technology_section_contentnine',TRUE),
                'technology_section_status' => 'enable',
                'technology_section_updated_by' => '1',
                );
   
                

                $data['where'] = array('technology_section_id' => 1);     
                $data['table'] = 'technology_section';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/technology/edit');
            }else{        
                $data['where'] = array('technology_section_id' => 1);     
                $data['table'] = 'technology_section';    
                $data['output_type'] = 'row';   
                $content['title'] = 'technology section'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'technology/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('technology_section_id' => 1);     
            $data['table'] = 'technology_section';    
            $data['output_type'] = 'row';   
            $content['title'] = 'technology section'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'technology/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>