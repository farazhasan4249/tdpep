<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends Admin_Controller {
    function __construct() {
    parent::__construct();		
    }  

    public function index(){ 
        if($_POST){
            $this->form_validation->set_rules('history_text', 'History Text', 'required');
            $this->form_validation->set_rules('history_heading', 'History Heading', 'required');

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'history_text' => $this->input->post('history_text',TRUE),
                'history_img' => $this->input->post('history_img',TRUE),
                'history_heading' => $this->input->post('history_heading',TRUE),
                'history_status' => 'enable',
                'history_updated_by' => '1',

                );    
                if($_FILES['history_img']['size'] > 0){
                    $image = single_image_upload($_FILES['history_img'],'./uploads/cms');
                    if(is_array($image)){            
                        $this->session->set_flashdata('error', $image);
                    }else{
                        $content['history_img'] = $image;
                    }
                } 

                $data['where'] = array('history_id' => 1);		
                $data['table'] = 'history';	
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/history');
            }else{        
                $data['where'] = array('history_id' => 1);		
                $data['table'] = 'history';	
                $data['output_type'] = 'row';	
                $content['title'] = 'History';	
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'history/edit';			
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('history_id' => 1);		
            $data['table'] = 'history';	
            $data['output_type'] = 'row';	
            $content['title'] = 'History';	
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'history/edit';			
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
