<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function edit()
    {   
      if($_POST){
       
            $this->form_validation->set_rules('terms_heading', 'Terms heading', 'required');
            $this->form_validation->set_rules('terms_subheading', 'Terms Sub heading', 'required');

            $this->form_validation->set_rules('terms_content', 'Terms Content', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'terms_heading' => $this->input->post('terms_heading',TRUE),
                'terms_subheading' => $this->input->post('terms_subheading',TRUE),
                'terms_content' => $this->input->post('terms_content',TRUE),
                'terms_status' => 'enable',
                'terms_updated_by' => '1',
                );    
                

                $data['where'] = array('terms_id' => 1);     
                $data['table'] = 'terms';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/terms/edit');
            }else{        
                $data['where'] = array('terms_id' => 1);     
                $data['table'] = 'terms';    
                $data['output_type'] = 'row';   
                $content['title'] = 'Terms'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'terms/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('terms_id' => 1);     
            $data['table'] = 'terms';    
            $data['output_type'] = 'row';   
            $content['title'] = 'Terms'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'terms/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>