<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_section extends Admin_Controller {
    function __construct() {
    parent::__construct();  

    $this->load->model('general', 'g'); 
    }  

    public function edit()
    {   
      if($_POST){
       
            $this->form_validation->set_rules('about_section_heading', 'About Sec heading', 'required');
            $this->form_validation->set_rules('about_section_text', 'About Sec text', 'required');
            

            if (!$this->form_validation->run() == FALSE){

                $content = array(
                'about_section_heading' => $this->input->post('about_section_heading',TRUE),
                'about_section_text' => $this->input->post('about_section_text',TRUE),
                'about_section_status' => 'enable',
                'about_section_updated_by' => '1',
                );    
                

                $data['where'] = array('about_section_id' => 1);     
                $data['table'] = 'about_section';    
                $this->general->update($data,$content);      
     
                $this->session->set_flashdata('success', 'Updated Successfully.');
                redirect('admin/About_section/edit');
            }else{        
                $data['where'] = array('about_section_id' => 1);     
                $data['table'] = 'about_section';    
                $data['output_type'] = 'row';   
                $content['title'] = 'about section'; 
                $content['record']  = $this->general->get($data);
                $content['main_content'] = 'About_section/edit';         
                $this->load->view('admin/inc/view',$content);   
            } 
        }else{        
            $data['where'] = array('about_section_id' => 1);     
            $data['table'] = 'about_section';    
            $data['output_type'] = 'row';   
            $content['title'] = 'about section'; 
            $content['record']  = $this->general->get($data);

            $content['main_content'] = 'about_section/edit';         
            $this->load->view('admin/inc/view',$content);   
        } 
    }
}
?>