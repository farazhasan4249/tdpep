<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Home extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
        $data = array();
        $data['table'] ='home';
        $data['output_type'] = 'result';
        $content['home'] = $this->general->get($data);

        $data = array();
        $data['table'] ='newarrival';
        $data['output_type'] = 'result';
        $content['newarrival']= $this->general->get($data);
        
        $data = array();
        $data['table'] ='product';
        $data['output_type'] = 'row';
       // $data['where'] = array('product_id' => $product_id);
        $content['product']= $this->general->get($data);

       
		$content['main_content'] = 'home';
		$this->load->view('front/inc/view',$content);
        
	}
}




?>