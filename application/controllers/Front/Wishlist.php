<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Wishlist extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
        //$data['where'] = array('wishlist.customer_id' => $_SESSION['customer_id']);
		$data['table'] = 'wishlist';
		$data['join_table'] = 'product';
		$data['join'] = 'product.product_id = wishlist.product_id';
		$data['join_type'] = 'left';
		//$data['order_by_col'] = 'wishlist.wishlist_created_at';
		// $data['table'] = "product";
		$data['output_type'] = "result";
        $content['records'] = $this->general->get($data);
		$content['main_content'] = 'wishlist';
		$this->load->view('front/inc/view',$content);
        }

         public function remove()
        {      
            $data['rowid'] = $this->input->post('rowid',TRUE);
            $data['qty'] = 0;
            $this->cart->update($data);
        }

       public function wish_remove()
	{
		$product_id = $this->uri->segment(3);
		$customer_id = $this->session->userdata('customer_id');
		$wishlist_id = $this->uri->segment(4);
		$this->general->delete_data('wishlist','',array('product_id'=>$product_id,'customer_id'=>$customer_id,'wishlist_id'=>$wishlist_id));
		$this->session->set_flashdata('msg', '1');
		$this->session->set_flashdata('alert_data', 'Successfully Removed');
		redirect($_SERVER['HTTP_REFERER']);
	}

        public function Wish()
        {
	 // print_r('a');exit;

		if($this->session->userdata('customer_email'))
		{
			$wish_pro_id = $this->general->get_single_field('wishlist',array('product_id'=>$_POST['product_id']),'product_id');
			$wish_cus_id = $this->general->get_single_field('wishlist',array('customer_id'=> $this->session->userdata('customer_id')),'customer_id');
			if($wish_pro_id == $_POST['product_id'] && $wish_cus_id == $this->session->userdata('customer_id'))
			{				
				echo 3;
			}
			else
			{
				$data['product_id'] = $_POST['product_id'];
				$data['customer_id'] = $this->session->userdata('customer_id');

				$result = $this->general->add_data('wishlist',$data);			
				if($result){	
					echo 1;
				}else{			
					echo 2;
				}
			}
		}
		else
		{
			echo 4;	
		}	
	}
}
?>