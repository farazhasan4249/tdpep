<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Dashboard extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{


        $data['table'] = 'customer';
        $data['output_type'] = "row";
		$content['customer'] = $this->general->get($data);
		$content['main_content'] = 'dashboard';
		$this->load->view('front/inc/view',$content);
        
	}
}
?>