<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Subcategory extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index($slug = "")
	{
	$pages = $this->input->get('per_page');
    $per_page = 21;    
    $this->load->library('pagination');
    // $total_row = $this->db->where('sub_category_status','enable')->get('sub_category')->num_rows();

    $config['base_url'] = base_url().'subcategory/'.$slug;
    $sub_category_id = get_id_by_slug('sub_category',$slug);
    $config['total_rows'] = $this->db->where('sub_category_id',$sub_category_id)->get('product')->num_rows();
    $config['per_page'] = 21;
    $config['uri_segment'] = 3;
    $config['num_links'] = 4;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    // $config['full_tag_open'] = '<ul class="pagination-d">';
    // $config['full_tag_close'] = '</ul>';
    // $config['first_tag_open'] = '<li>';
    // $config['first_tag_close'] = '</li>';
    // $config['last_tag_open'] = '<li>';
    // $config['last_tag_close'] = '</li>';
    // $config['next_link'] = '&gt;';
    // $config['next_tag_open'] = '<li>';
    // $config['next_tag_close'] = '</li>';
    // $config['prev_link'] = '&lt;';
    // $config['prev_tag_open'] = '<li>';
    // $config['prev_tag_close'] = '</li>';
    // $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    // $config['cur_tag_close'] = '</a></li>';
    // $config['num_tag_open'] = '<li>';
    // $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(3);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];

    // echo pagenation(base_url().'subcategory/'.$slug,$per_page,$total_row);

		$sub_category_id = get_id_by_slug('sub_category',$slug);

		if(!empty($sub_category_id)){
		$data = array();
        $data['table'] = 'category';
        $data['join_table'] = 'sub_category';
        $data['join'] = 'category.category_id = sub_category.category_id';
        $data['join_type'] = 'left';
        $data['output_type'] = "result";
        $content['category'] = $this->general->get($data);
        $content['sub_category'] = $this->general->get($data);

        $data = array();
        $data['table'] = 'newarrival';
        $data['output_type'] = "result";
        $content['newarrival'] = $this->general->get($data);
		}
		else{
			redirect();
		}
		$data=array();
		if(!empty($sub_category_id)){
			$data['table'] = 'product';
			$data['limit'] = $per_page;
            $data['offset'] = $pages;
			$data['output_type']='result';
			$data['order_by_col']='product_name';
			$data['where'] = array('sub_category_id' => $sub_category_id);
			$content['product'] = $this->general->get($data);
		}
		else{
			redirect();
		}
		$content['main_content']='subcat-product';
		$this->load->view('front/inc/view',$content);

	}
}
?>