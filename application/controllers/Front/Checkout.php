<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Checkout extends Front_Controller
{ 
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{   

		if($this->input->post()){

			$content = array(
				
				'order_fname' => $this->input->post('order_fname',TRUE),
				'order_lname' => $this->input->post('order_lname',TRUE),
				'order_email' => $this->input->post('order_email',TRUE),
				'order_company' => $this->input->post('order_company',TRUE),
				'order_country' => $this->input->post('order_country',TRUE),
				'order_streetaddress_one' => $this->input->post('order_streetaddress_one',TRUE),
				'order_streetaddress_two' => $this->input->post('order_streetaddress_two',TRUE),
				'order_city' => $this->input->post('order_city',TRUE),
				'order_state' => $this->input->post('order_state',TRUE),
				'order_postcode' => $this->input->post('order_postcode',TRUE),
				'order_phone' => $this->input->post('order_phone',TRUE),
				'order_note' => $this->input->post('order_note',TRUE),
				'order_price' => $this->input->post('order_price',TRUE),
				'order_status' => 'enable',
				'order_created_by' => '1',

			);   

			if($this->input->post('order_price',TRUE)){
				$content['order_price'] = $this->input->post('order_price',TRUE);
			}
			else if($this->input->post('product_discounted_price',TRUE)){
				$content['order_price'] = $this->input->post('product_discounted_price',TRUE);
			}

			
			$data['table'] = 'order';    
			$id = $this->general->insert($data,$content);   
			
			$this->session->set_flashdata('success', 'Updated Successfully.');
			redirect('https://api-cert.payeezy.com/v1/transactions/'.$id);
			
		}
		else {
			$data['table'] = "order";
			$data['output_type'] = "row";
			$content['order'] = $this->general->get($data);
			$content['price'] = $this->session->userdata('price');

			$content['main_content'] = 'checkout'; 
			$this->load->view('front/inc/view',$content);
		}


	}

	public function Add()
	{
		$data['table'] = 'order';
		$data['output_type'] = 'row';
		$content['order'] = $this->general->get($data);
		$content['main_content'] = 'checkout';
		$this->load->view('front/inc/view',$content);
	}
}
?>