<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Terms extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
       $data = array();
        $data['table'] = "terms";
        $data['output_type'] = "row";
        $content['terms'] = $this->general->get($data);

		$content['main_content'] = 'term-condition';
		$this->load->view('front/inc/view',$content);
        
	}
}
?>