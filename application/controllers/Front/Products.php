<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Products extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
	$pages = $this->input->get('per_page');
    $per_page = 21;    
    $this->load->library('pagination');
    $total_row = $this->db->where('product_status','enable')->get('product')->num_rows();

    $config['base_url'] = base_url().'products';
    $config['total_rows'] = $this->db->where('product_status','enable')->get('product')->num_rows();
    $config['per_page'] = 21;
    $config['uri_segment'] = 2;
    $config['num_links'] = 4;
    // $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    // $config['full_tag_open'] = '<ul class="pagination-b">';
    // $config['full_tag_close'] = '</ul>';
    // $config['first_tag_open'] = '<li>';
    // $config['first_tag_close'] = '</li>';
    // $config['last_tag_open'] = '<li>';
    // $config['last_tag_close'] = '</li>';
    // $config['next_link'] = '&gt;';
    // $config['next_tag_open'] = '<li>';
    // $config['next_tag_close'] = '</li>';
    // $config['prev_link'] = '&lt;';
    // $config['prev_tag_open'] = '<li>';
    // $config['prev_tag_close'] = '</li>';
    // $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    // $config['cur_tag_close'] = '</a></li>';
    // $config['num_tag_open'] = '<li>';
    // $config['num_tag_close'] = '</li>';
    $offset = $this->uri->segment(2);
    $this->pagination->initialize($config);
    $page = $this->input->get('per_page');
    $start = ($page - 1) * $config['per_page'];
    $data["links"] = $this->pagination->create_links();

    // echo pagenation(base_url().'products',$per_page,$total_row);
       
      
        $data = array();
        $data['table'] = 'category';
        $data['join_table'] = 'sub_category';
        $data['join'] = 'category.category_id = sub_category.category_id';
        $data['join_type'] = 'left';
        $data['output_type'] = "result";
        $content['category'] = $this->general->get($data);
        $content['sub_category'] = $this->general->get($data);
        
        $data = array();
        $data['table'] = 'product';
        $data['limit'] = $per_page;
        $data['offset'] = $pages;
        $data['output_type'] = "result";
        $content['product'] = $this->general->get($data);

        $data = array();
        $data['table'] = 'newarrival';
        $data['output_type'] = "result";
        $content['newarrival'] = $this->general->get($data);

		$content['main_content'] = 'products';
		$this->load->view('front/inc/view',$content, $data);
        
	}

	public function detail()
	{
		$data['table'] = 'product';
        $data['output_type'] = "row";
        $content['product'] = $this->general->get($data);

		$content['main_content'] = 'product-detail';
		$this->load->view('front/inc/view',$content);
	}

}
?>