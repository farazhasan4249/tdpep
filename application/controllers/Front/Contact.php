<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Contact extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{

		if($this->input->post()){
			
             // echo "<pre>";
             // print_r($this->input->post());die();

			$this->form_validation->set_rules('contact_fname', 'First Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_lname', 'Last Name', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_email', 'Email', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_password', 'Password', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('contact_subject', 'Subject', 'required|min_lenght[2]|max_length[50]');
			$this->form_validation->set_rules('contact_phone', 'Phonenum', 'required|min_length[9]|max_length[15]');
			$this->form_validation->set_rules('contact_message', 'Message', 'required|min_length[3]|max_length[300]');

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					'contact_fname' => $this->input->post('contact_fname',TRUE),
					'contact_lname' => $this->input->post('contact_lname',TRUE),
					'contact_email' => $this->input->post('contact_email',TRUE),
					'contact_password' => $this->input->post('contact_password',TRUE),
					'contact_subject' => $this->input->post('contact_subject',TRUE),
					'contact_phone' => $this->input->post('contact_phone',TRUE),
					'contact_message' => $this->input->post('contact_message',TRUE),
					'contact_status' => 'enable',
					'contact_created_by' => '1',
				); 
				//print_r($content);exit;


				$data['table'] = 'contact';
				$email = $this->input->post('contact_email');
				$headers = 'From: '.$email;
				$to_email = 'fh@nadocrm.com';
				$subject = 'Contact Info';
				$message = '
				First Name : ' . $this->input->post("contact_fname", TRUE) . '<br>
				Last Name : ' . $this->input->post("contact_lname", TRUE) . '<br>
				Email : ' . $this->input->post("contact_email", TRUE) . '<br>
				Phone : ' . $this->input->post("contact_phone", TRUE) . '<br>
				Password : ' . $this->input->post("contact_password", TRUE) . '<br>
				Subject : ' .$this->input->post("contact_subject", TRUE) . '<br>
				Message : ' . $this->input->post("contact_message", TRUE) . '<br>
				';
				
				send_email($to_email,$subject,$message,$headers); 

				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
        
		$content['main_content'] = 'contact';
		$this->load->view('front/inc/view',$content);
        
	}
}
?>