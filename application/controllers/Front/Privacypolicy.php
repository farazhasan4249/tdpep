<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Privacypolicy extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{

		 $data = array();
        $data['table'] = "privacy";
        $data['output_type'] = "row";
        $content['privacy'] = $this->general->get($data);
       
		$content['main_content'] = 'privacy-policy';
		$this->load->view('front/inc/view',$content);
        
	}
}
?>