<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Cart extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
       // $content['productdata']=$this->cart->contents();
        $data['table'] = "product";
        $content['product'] = $this->general->get($data);
		$content['main_content'] = 'cart';
		$this->load->view('front/inc/view',$content,$data);
        
	}

	 public function remove()
        {      
            $data['rowid'] = $this->input->post('rowid',TRUE);
            $data['qty'] = 0;
            $this->cart->update($data);
        }
        
       public function update()
        {  
            
            if(!empty($this->input->post('qty'))){
                $data = array(
                    'rowid' => $this->input->post('rowid'),
                    'qty'   => $this->input->post('qty')
                );
                $this->cart->update($data);

                $ttl = 0;
                foreach($this->cart->contents() as $items){
                    $ttl += $items['qty'] * $items['price'];
                }

                $cart['content'] = $this->cart->contents();
                $cart['ttl'] = $ttl;

                $carttable = $this->load->view('front/cart');

                echo json_encode($carttable);
            }else{
                redirect('cart');
            }
        
        }
        
        public function minicart()
	{		
		echo count($this->cart->contents());
	}

     public function insert(){
        $id = $this->input->post('id',TRUE);
        $qty = $this->input->post('qty',TRUE);
        //$price = $this->input->post('price',TRUE);
        // $name = $this->input->post('name',TRUE);
        
        $reg_price = $this->general->get_single_field('product',array('product_id'=>$id),'product_reg_price');
		$dis_price = $this->general->get_single_field('product',array('product_id'=>$id),'product_discounted_price');
		$price = ''.($dis_price > 0?$dis_price:$reg_price).''; 
		
        $image = $this->general->get_single_field('product',array('product_id'=>$id),'product_image');
        
        // $description = $this->input->post('description',TRUE);
       // $vendor_id = $this->general->get_single_field('product' ,array('product_id'=>$id),'user_id');   
        
        
        $data=array(
            'id' => $id,
            'qty' => $qty,
            'image' => $image,
            'price' => $price
            
           // 'vendor_id' => $vendor_id
        );
        $this->cart->insert($data);
    }

        public function add()
        {  
            $id = $this->input->post('id',TRUE);
             
            $data['table'] = "product";
            $data['where'] = array('product_id'=>$id);
            $content['product'] = $this->general->get($data);
           
            $reg_price = $this->general->get_single_field('product',array('product_id'=>$id),'product_reg_price');
	    	$dis_price = $this->general->get_single_field('product',array('product_id'=>$id),'product_discounted_price');
	    	$price = ''.($dis_price > 0?$dis_price:$reg_price).''; 
            
            $image = $this->general->get_single_field('product',array('product_id'=>$id),'product_image');
            $product_name = $this->input->post('product_name',TRUE);
            $data = array(
                    'id'      => $id,
                    'qty'     => 1,
                    'image'   => $image,
                    'price'   => $price,
                    'name'    => $product_name,
                    'options' => array('product' => [])
            );
            
            $result = $this->cart->insert($data);
        
            if($result){            
                 $output['status'] = 'success';
                 $output['count'] = $this->cart->total_items();
             }
             echo json_encode($output);
        }
    }


?>