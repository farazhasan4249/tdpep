<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Newsletter extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index(){
		
		$content['main_content'] = "home";
		$this->load->view('front/inc/view',$content);

		if($this->input->post()){
			
             // echo "<pre>";
             // print_r($this->input->post());die();

			
			$this->form_validation->set_rules('newsletter_email', 'Email', 'required|valid_email');
			

			if (!$this->form_validation->run() == FALSE){
				
				$content = array(
					
					'newsletter_email' => $this->input->post('newsletter_email',TRUE),
					'newsletter_status' => 'enable',
					'newsletter_created_by' => '1',

				); 
                $data['table'] = 'newsletter';
				
				$this->general->insert($data,$content);      

				$this->session->set_flashdata('success', 'Updated Successfully.');
				redirect('');
			}else{        
				redirect('');

			} 
		}
	}
}


?>

