<?php
defined('BASEPATH') OR exit('no direct script access allowed');
/**
* 
*/
class Categories extends Front_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function index($slug="")
	{
		$data = array();
		
		$data['table'] = "category";
		$data['output_type'] = "result";
		$content['categories'] = $this->general->get($data);

		$data = array();

		$data['table'] = 'product';
		$data['output_type']= 'result';
		$data['order_by_col']='product_name';
		$content['product'] = $this->general->get($data);
		
		$content['main_content']='categories';
		$this->load->view('front/inc/view',$content);
	}
}
?>