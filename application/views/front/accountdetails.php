
<div class="dashBoard">
     <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
             
             <a href="<?php echo base_url('dashboard');?>" class="dashboardbtn">Dashboard</a>
             <a href="<?php echo base_url('accountdetails');?>" class="dashboardbtn active">Account Details</a>
             <a href="<?php echo base_url('orderhistory')?>" class="dashboardbtn">Order History</a>
             
             <a href="<?php echo base_url('register/logout')?>" class="dashboardbtn">Log Out</a>
          </div>
          
          
          <div class="col-12 col-sm-7 col-md-8 col-lg-8">
          <h2 class="text-left dashBoardH4">My Account Details</h2>
         
            <div class="mainForm">
      
              <form>
              <div class="row">
                <div class="col-12 col-sm-6 dashboardform">
                  <form action="<?php echo base_url('register/updatequery');?>" method="post">

                <div class="col-12 col-sm-12 margin-bottom-5"><h4>Account Information</h4></div>

                <!-- <div class="col-12 col-sm-12">
            
              <label>First Name</label>
              <input type="text" placeholder="Enter Your First Name">
            </div> -->
                <div class="col-12 col-sm-12">
            
              <label>Name</label>
              <input id="customer_name" name="customer_name" type="text" placeholder="Enter Your Name">
            </div>
                <div class="col-12 col-sm-12">
            
              <label>Email</label>
              <input id="customer_email" name="customer_email" type="email" placeholder="Enter Your Email">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 text-center">
                  <a href="<?php echo base_url('register/updatequery');?>" class="dashboardbtn2">Update Details</a>
                </div>
              </form>
                </div>
                
                <div class="col-xs-12 col-sm-6">
                <div class="col-xs-12 col-sm-12 margin-bottom-5"><h4>Set New Password</h4></div>
                <div class="col-xs-12 col-sm-12">
            
              <label>Current Password</label>
              <input id="customer_password" name="customer_password" type="password" placeholder="Enter Your Current Password">
            </div>
                <div class="col-xs-12 col-sm-12">
            
              <label>New Password</label>
              <input id="customer_password" name="customer_password" type="password" placeholder="Enter Your New Password">
            </div>
                <div class="col-xs-12 col-sm-12">
            
              <label>Confirm Password</label>
              <input type="password" placeholder="Confirm Your New Password">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 text-center">
                <a href="<?php  echo  base_url('login/update_password')?>" class="dashboardbtn2">Update Password</a>
                </div>
                </div>
                </div>
                </form>
      
             

                <form>
                
                </form>

       
                
                
                
                
            </div>
            </div>
    
          </div>
     </div>
</div>
</div>

<script src="js/bootstrap.js"></script>
</body>
</html>
