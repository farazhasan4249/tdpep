

<body>

    <section>
        <div class="login-page">
            <div class="container">
                <div class="row">
                       <div class="col-12 col-sm-5 col-md-5 col-lg-5 center">
                        <div class="login-box">
                            <img src="<?php echo base_url('assets/front/images');?>/logo.png" class="img-fluid">
                           
                           
                           <span class="input input--nao">
                        <input class="input__field input__field--nao" type="text" id="input-1">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">First Name</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="text" id="input-1">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Last Name</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                           
                           
                           
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="text" id="input-1">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Email</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="password" id="input-1">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Password</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            
                            
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="text" id="input-1">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Contact Number</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            
                            <button>sign in</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>