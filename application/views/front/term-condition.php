
<body>
   <div class="cursor"></div>
   <!-- Back to top button -->
   <a id="button"></a>

   <div class="inner-banner-sec">
      <div class="container">
         <div class="terms-banner">
         </div>
      </div>
   </div>


   <section>
      <div class="terms-condition">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h5><?php echo base_url($terms->terms_heading)?$terms->terms_heading:''?></h5>
                  <h4><?php echo base_url($terms->terms_subheading)?$terms->terms_subheading:''?></h4>
                  <p><?php echo base_url($terms->terms_content)?$terms->terms_content:''?></p>
                 
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
   <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
   <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
   <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
   <script>
      AOS.init();
   </script>
</body>
</html>