
   <body>
      
      <!-- Back to top button -->
      <a id="button"></a>
     
      <div class="inner-banner-sec">
         <div class="container">
            <div class="inner-banner">
               <div class="inner-banner-txt">
                  <h4>whishlist</h4>
               </div>
            </div>
         </div>
      </div>
      <div class="cart-main-wrapper pt-60 pb-60">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <!-- Cart Table Area -->
                  <div class="cart-table table-responsive">
                     <table class="table table-bordered">
                        <thead>
                           <tr>
                              <th class="pro-thumbnail">Thumbnail</th>
                              <th class="pro-title">Product</th>
                              <th class="pro-price">Price</th>
                              <th class="pro-quantity">Quantity</th>
                              <th class="pro-subtotal">Total</th>
                              <th class="pro-remove">Remove</th>
                           </tr>
                        </thead>
                        <tbody>
                              <?php if(isset($records) && !empty($records)) { ?>
                              <?php foreach ($records as $record) { ?>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="<?php echo ('').$record->product_image?>" alt="Product"></a></td>
                              <td class="pro-title"><a href="#"><?php echo base_url('$record->product_name')?$record->product_name:''?></a></td>
                              <td class="pro-price"><span>$<?php echo base_url('$record->product_discounted_price')?$record->product_discounted_price:''?></span></td>
                              <td class="pro-quantity">
                                 <span class="text-success">In Stock</span>
                              </td>
                              <td class="pro-subtotal"><span>$<?php echo base_url('$record->product_discounted_price')?$record->product_discounted_price:''?></span></td>
                              <td class="pro-remove"><button id="<?php echo !empty($items['rowid'])?$items['rowid']:'';?>"><i class="btn btn-danger fa fa-trash-o cart-remove-item"></i></button></td>
                           </tr>
                              <?php }} ?>
                          
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>


 
      <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>

       <script>
         
         $(document).ready(function(){
         $(".cart-remove-item").on('click',function(){
            //alert('a');
        var x = $('.cart-remove-item').length;
        var temp = $(this).parents('tr');
        $.ajax({
          url: "<?php echo base_url('cart/remove') ?>",  
          type: "POST",
          data: {rowid:$(this).data('id')},   
          dataType: "html",     
      success: function(data){
          if(x == 1){
              window.location.reload();
          }else{
             $(temp).remove(); 
          }
                toastr.success('Updated Successfully')
      },
      error: function(data) {
                toastr.error('Something Went Wrong, Please Try Again Later.')
      },
    }); 
    });
         });
    </script>

      <script>
         AOS.init();
      </script>
   </body>
</html>