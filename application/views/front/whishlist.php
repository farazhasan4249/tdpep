
   <body>
      <div class="cursor"></div>
      <!-- Back to top button -->
      <a id="button"></a>
     
      <div class="inner-banner-sec">
         <div class="container">
            <div class="inner-banner">
               <div class="inner-banner-txt">
                  <h4>whishlist</h4>
               </div>
            </div>
         </div>
      </div>
      <div class="cart-main-wrapper pt-60 pb-60">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <!-- Cart Table Area -->
                  <div class="cart-table table-responsive">
                     <table class="table table-bordered">
                        <thead>
                           <tr>
                              <th class="pro-thumbnail">Thumbnail</th>
                              <th class="pro-title">Product</th>
                              <th class="pro-price">Price</th>
                              <th class="pro-quantity">Quantity</th>
                              <th class="pro-subtotal">Total</th>
                              <th class="pro-remove">Remove</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="assets/images/shop-img-1.png" alt="Product"></a></td>
                              <td class="pro-title"><a href="#">Endeavor Daytrip</a></td>
                              <td class="pro-price"><span>$295.00</span></td>
                              <td class="pro-quantity">
                                 <span class="text-success">In Stock</span>
                              </td>
                              <td class="pro-subtotal"><span>$295.00</span></td>
                              <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                           </tr>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="assets/images/shop-img-1.png" alt="Product"></a></td>
                              <td class="pro-title"><a href="#">Joust Duffle Bags</a></td>
                              <td class="pro-price"><span>$275.00</span></td>
                              <td class="pro-quantity">
                                 <span class="text-success">In Stock</span>
                              </td>
                              <td class="pro-subtotal"><span>$550.00</span></td>
                              <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                           </tr>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="assets/images/shop-img-1.png" alt="Product"></a></td>
                              <td class="pro-title"><a href="#">Compete Track Totes</a></td>
                              <td class="pro-price"><span>$295.00</span></td>
                              <td class="pro-quantity">
                                 <span class="text-danger">Out Stock</span>
                              </td>
                              <td class="pro-subtotal"><span>$295.00</span></td>
                              <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                           </tr>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="assets/images/shop-img-1.png" alt="Product"></a></td>
                              <td class="pro-title"><a href="#">Bess Yoga Shorts</a></td>
                              <td class="pro-price"><span>$110.00</span></td>
                              <td class="pro-quantity">
                                 <span class="text-success">In Stock</span>
                              </td>
                              <td class="pro-subtotal"><span>$110.00</span></td>
                              <td class="pro-remove"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
 
      <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
      <script>
         AOS.init();
      </script>
   </body>
</html>