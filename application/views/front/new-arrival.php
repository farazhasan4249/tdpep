
<body>
   <!-- <div class="cursor"></div> -->
   <!-- Back to top button -->
   <a id="button"></a>
  
   <div class="banner-sec">
      <div class="container">
         <div class="banner-image">
            <img src="<?php echo base_url('assets/front/images');?>/new-arival-banner.jpg" alt="banner-img" class="img-fluid"/>
            
         </div>
      </div>
   </div>
   
   
   <section class="stock-deals">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
            <div class="new-arival">
            <img src="<?php echo base_url('assets/front/images');?>/new-ari.png" class="img-fluid center">
<h5>new arrival</h5>
            </div>
            </div>
         </div>
         <div class="row">
            <?php if(isset($newarrival) && !empty($newarrival)) { ?>
            <?php foreach($newarrival as $new) { ?>
            <div class="col-md-2">
               <div class="deal-pr-box">
                  <p class="deal-pr-box-p"><?= $new->newarrival_name;?></p>
                  <img src="<?php echo base_url('uploads/settings/').$new->newarrival_image;?>" alt="deal-pro-img" class="img-fluid" />
                  <div class="cart-box">
                     <a href="<?php echo base_url('products/detail')?>"><img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid"></a>
                  </div>
               </div>
            </div>
            <?php }} ?>
         </div>
      </div>
   </section>
   

<script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
<script>
   AOS.init();
</script>
</body>
</html>