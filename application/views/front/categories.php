
      <div class="inner-banner-sec">
         <div class="container">
            <div class="inner-banner">
               <div class="inner-banner-txt">
                  <h4>categories</h4>
               </div>
            </div>
         </div>
      </div>
   <section class="categories-sec">
      <div class="container">
         <div class="row">
             <?php if(isset($categories) && !empty($categories)) { ?>
            <?php foreach($categories as $category) { ?>
            <div class="col-md-4">
               <div class="cat-box">
                  <a href="<?php echo base_url('category/').$category->category_slug;?>"><img src="<?php echo base_url('uploads/settings/').$category->category_image;?>" alt="image" class="img-fluid" /></a>
                  <div class="cat-txt">
                     <h6><?php echo base_url($category->category_name)?$category->category_name:''?></h6>
                     <p><?php echo get_cat_count($category->category_id);?></p>
                  </div>
               </div>
            </div>
            <?php }}?>
         </div>
        <!--  <div class="paginations">
            <?php echo $this->pagination->create_links();?>
          </div> -->
      </div>
   </section>
<script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
   <script>
      AOS.init();
   </script>
</body>
</html>