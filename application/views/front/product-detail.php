
<body>
   <!-- Back to top button -->
   <a id="button"></a>


      <div class="inner-banner-sec">
         <div class="container">
            <div class="inner-banner">
               <div class="inner-banner-txt">
                  <h4>product detail</h4>
               </div>
            </div>
         </div>
      </div>

   <section class="prod-detail-sec">
      <div class="card">

         <div class="container">
            <div class="wrapper row">
               <div class="preview col-md-6">
                  <div class="preview-pic tab-content">
                     <div class="tab-pane active" id="pic-1"><img src="<?php echo base_url('uploads/products/'.$product->product_image.'.jpg');?>" alt="shop-img-1" class="img-fluid" /></div>
                  
                  </div>
                  <ul class="preview-thumbnail nav nav-tabs">
                    
                  </ul>
               </div>
               <div class="details col-md-6">
                  <h3 class="product-title"><?php echo base_url('$product->product_name')?$product->product_name:''?></h3>
                  <div class="rating">
                    <!--  <div class="stars">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                     </div> -->
                     <!--<span class="review-no">41 reviews</span>-->
                  </div>
                  <p class="product-description"><?php echo base_url('$product->product_short_desc')?$product->product_short_desc:''?></p>
                  <div>
                     <p>$<?php echo base_url($product->product_discounted_price)?$product->product_discounted_price:''?></p>
                  </div>
                  <div class="action">
                     <form action="<?php echo base_url('cart')?>" id="cart_submit" method="post">
                    <input type="hidden" value="<?php echo $product->product_id;?>" name="id">
                     <input type="hidden" value="<?php echo $product->product_name;?>" name="product_name">
                      <input type="hidden" value="<?php echo $product->product_discounted_price > 0?$product->product_discounted_price:'';?>" id="actual_price" name="product_discounted_price">
                     <button class="add-to-cart-inner btn btn-default cart_form" id="carts" type="submit">add to cart</button>
                    </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="prod-detail-tabs">
            <div class="container">
               <div class="row">
                  <div class="col-md-3">
                     <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                           <!--<a class="nav-link active" data-toggle="pill" href="#descrip">Description</a>-->
                        </li>
                        <!--<li class="nav-item">-->
                        <!--   <a class="nav-link" data-toggle="pill" href="#review">Reviews</a>-->
                        </ul>
                     </div>
                     <div class="col-md-9">
                        <!-- Tab panes -->
                        <!--<div class="tab-content">-->
                        <!--   <div id="descrip" class="container tab-pane active">-->
                        <!--      <br>-->
                        <!--      <p><?php echo base_url('$product->product_long_desc')?$product->product_long_desc:''?>-->
                        <!--      </p>-->
                        <!--   </div>-->
                           <!--<div id="review" class="container tab-pane fade">-->
                           <!--   <br>-->
                           <!--   <div class="row">-->
                           <!--      <div class="col-md-12">-->
                           <!--         <div class="review-box">-->
                           <!--            <img src="<?php echo base_url('assets/front/images');?>/review-img.png" alt="review-img" class="img-fluid" />-->
                           <!--            <div class="review-box-comm">-->
                           <!--               <div class="rating">-->
                           <!--                  <div class="stars">-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                  </div>-->
                           <!--                  <span class="review-no">41 reviews</span>-->
                           <!--               </div>-->
                           <!--               <h6>ADMIN - <span>30 Nov, 2018</span></h6>-->
                           <!--               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod-->
                           <!--                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.-->
                           <!--               </p>-->
                           <!--            </div>-->
                           <!--         </div>-->
                           <!--         <div class="review-box">-->
                           <!--            <img src="<?php echo base_url('assets/front/images');?>/review-img.png" alt="review-img" class="img-fluid" />-->
                           <!--            <div class="review-box-comm">-->
                           <!--               <div class="rating">-->
                           <!--                  <div class="stars">-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                  </div>-->
                           <!--                  <span class="review-no">41 reviews</span>-->
                           <!--               </div>-->
                           <!--               <h6>ADMIN - <span>30 Nov, 2018</span></h6>-->
                           <!--               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod-->
                           <!--                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.-->
                           <!--               </p>-->
                           <!--            </div>-->
                           <!--         </div>-->
                           <!--         <div class="review-box">-->
                           <!--            <img src="<?php echo base_url('assets/front/images');?>/review-img.png" alt="review-img" class="img-fluid" />-->
                           <!--            <div class="review-box-comm">-->
                           <!--               <div class="rating">-->
                           <!--                  <div class="stars">-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star checked"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                     <span class="fa fa-star"></span>-->
                           <!--                  </div>-->
                           <!--                  <span class="review-no">41 reviews</span>-->
                           <!--               </div>-->
                           <!--               <h6>ADMIN - <span>30 Nov, 2018</span></h6>-->
                           <!--               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod-->
                           <!--                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.-->
                           <!--               </p>-->
                           <!--            </div>-->
                           <!--         </div>-->
                           <!--      </div>-->
                           <!--   </div>-->
                           <!--</div>-->
                        <!--</div>-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>




     
 
      <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
      <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

      <script type="text/javascript">
 
   $(document).ready(function(){
     $("#cart_submit").submit(function(e){
         $('#carts').attr('disabled','disabled');
      
     e.preventDefault();
     //alert('asd');
         var form = $(this);
         $.ajax({
       type: "POST",
       url: "<?php echo base_url('cart/add') ?>",  
       data: form.serialize(),
       dataType: "json",

       success: function(data){ 
       $('#carts').attr('disabled','disabled');
           if(data.status == 'success'){
             toastr.success('Added To Cart.');
                     $('.cart_popup').toggle();
                //  console.log(data.count);
                 minicart();
                 $('.cart-notification').text(data.count);    
           }else{
                     toastr.error(data.msg);   
           }
       },
       error: function(data) {
                 toastr.error('Something Went Wrong, Please Try Again Later.')
       },  
     });   
     });

    $(".cart_update").on('click',function(){
     // alert('asd');
        var rowid = [];
        var qty = [];
        var id = [];
        $('.cart-qty').each(function(i){
           qty.push($(this).val());
           rowid.push($(this).data('id'));
           id.push($(this).data('proid'));
        });
        $.ajax({
      type: "POST",
      url: "<?php echo base_url('cart/update') ?>",  
      data: {rowid:rowid,qty:qty,id:id},    
      dataType: "html", 
            success: function(data){
              console.log(data);
            
               window.location.reload();
      },
    }); 
     });  
     
 
    function minicart(){
   $.ajax({
     url: '<?php echo base_url("cart/minicart");?>',
     type: 'post',     
     success: function(data) {
      $('#minicart').html(data);
    }
  });
 }
minicart();
     
     
      
    
    $(".cart-remove-item").on('click',function(){
        var x = $('.cart-remove-item').length;
        var temp = $(this).parents('tr');
        $.ajax({
      type: "POST",
      url: "<?php echo base_url('cart/remove') ?>",  
      data: {rowid:$(this).data('id')},   
      dataType: "html",     
      success: function(data){
          if(x == 1){
              window.location.reload();
          }else{
             $(temp).remove(); 
          }
                toastr.success('Updated Successfully')
      },
      error: function(data) {
                toastr.error('Something Went Wrong, Please Try Again Later.')
      },
    }); 
    });

  });
</script>

      <script>
         AOS.init();
      </script>
   </body>
   </html>


   