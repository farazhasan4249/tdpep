<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TDPEP Naturals</title>
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    <link rel="icon" type="image/png" href="assets/images/fav-icon.png">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/aos.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/demo.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/set2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <section>
        <div class="login-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-5 col-md-5 col-lg-5 center">
                        <div class="login-box">
                    <form action="<?php echo base_url('login/loginquery')?>" method="post">
                            <img src="<?php echo base_url('assets/front/images');?>/logo.png" class="img-fluid">
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="text" id="customer_email" name="customer_email">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Email</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            <span class="input input--nao">
                        <input class="input__field input__field--nao" type="password" id="customer_password" name="customer_password">
                        <label class="input__label input__label--nao" for="input-1">
                           <span class="input__label-content input__label-content--nao">Password</span>
                            </label>
                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
                            </svg>
                            </span>
                            <div class="custom-radio001">
                                <ul>
                                    <li>
                                        <input type="radio" id="f-option" name="selector">
                                        <label for="f-option">Remember Me</label>
                                        <div class="check"></div>
                                    </li>
                                </ul>
                            </div>
                            <button>login</button>
                            <div class="login-footer">
                               <ul>
                                  <li><a href="<?php echo base_url('login/forgot_password');?>">forgot your password?</a></li>
                               </ul>
                            </div>
                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
    <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
    <script>
        AOS.init();
    </script>
</body>

</html>