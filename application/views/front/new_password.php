<div class="main-banner2 inner">
    <div class="container">
       <!--  <?php //include 'inc/navbar.php'; ?> -->
        <div class="banner-text">
            <h2>FORGOT PASSWORD</h2>
            <h1>TDPEP - Naturals</h1>
        </div>
    </div>
</div>
<div class="about-section inner">
    <div class="container">
        <div class="col-sm-6 col-sm-offset-3">
            <form action="<?php  echo  base_url('login/reset_password'); ?>" method="post">
                <input type="password" class="field" name="new_password" placeholder="New Password"/>
                <input type="password" class="field" name="confirm_password" placeholder="Confirm Password"/>
                <div class="text-center">
                    <input type="submit" class="sbmt" name="submit" value="Submit"/>
                </div>
            </form>
        </div>
    </div>
</div>
