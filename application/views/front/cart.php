
      <div class="inner-banner-sec">
         <div class="container">
            <div class="inner-banner">
               <div class="inner-banner-txt">
                  <h4>cart</h4>
               </div>
            </div>
         </div>
      </div>
      <div class="cart-main-wrapper pt-60 pb-60">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <!-- Cart Table Area -->
                  <div class="cart-table table-responsive">
                     <?php if(!empty($this->cart->contents())): ?>
                     <table class="table table-bordered">

                        <thead>
                           <tr>
                              <th class="pro-thumbnail">Thumbnail</th>
                              <th class="pro-title">Product</th>
                              <th class="pro-price">Price</th>
                              <th class="pro-quantity">Quantity</th>
                              <th class="pro-subtotal">Total</th>
                              <th class="pro-remove">Remove</th>
                           </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->cart->contents() as $items): ?>
                           <tr>
                              <td class="pro-thumbnail"><a href="#"><img class="img-fluid" src="<?= $items['image'] ?>" alt="Product"></a></td>
                              <td class="pro-title"><a href="#"><?php echo !empty($items['name'])?$items['name']:'' ?></a></td>
                              <td class="pro-price"><span>$<?php echo !empty($items['price'])?$items['price']:'';?></span></td>
                              <td class="pro-quantity">
                                  <input onkeyup="cart_updates(this)" type="text" data-id="<?php echo !empty($items['rowid'])?$items['rowid']:'';?>" data-proid="<?php echo !empty($items['id'])?$items['id']:'';?>" class="form-control cart_updates text-center input-text cart-qty qty" title="Qty" value="<?php echo !empty($items['qty'])?$items['qty']:'';?>" id="qty-cart" name="qty">
                              </td>
                              <td class="pro-subtotal"><span>$<?php echo !empty($items['subtotal'])?$items['subtotal']:'';?></span></td>
                              <td class="pro-remove"><i data-id="<?php echo !empty($items['rowid'])?$items['rowid']:'';?>" class="fa fa-trash-o cart-remove-item"></i></td>
                           </tr>
                             <?php endforeach; ?>
                          
                        </tbody>
         
                     </table>
                      <?php else: ?>
            <h2> No products found in add to cart </h2>
          <?php endif; ?>
                  </div>
                  <!-- Cart Update Option -->
                  <div class="cart-update-option d-block d-md-flex justify-content-between">
                    <!--  <div class="apply-coupon-wrapper">
                        <form action="#" method="post" class=" d-block d-md-flex">
                           <input type="text" placeholder="Enter Your Coupon Code" required="">
                           <button class="btn btn__bg btn__sqr">Apply Coupon</button>
                        </form>
                     </div> -->
                     <div class="cart-update mt-sm-16">
                        <a href="<?php echo base_url('cart/update')?>" class="btn btn__bg btn__sqr">Update Cart</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-5 ml-auto">
                  <!-- Cart Calculation Area -->
                  <div class="cart-calculator-wrapper">
                     <div class="cart-calculate-items">
                        <h3>Cart Totals</h3>
                        <div class="table-responsive">
                           <table class="table">
                              <tbody>
                            
                                 <tr class="total">
                                    <td>Total</td>
                                    <?php if(!empty($this->cart->contents())): ?> 
                                    <td class="total-amount">$<?php echo $this->cart->total();?></td>
                                    <?php else: ?>
                                    <td class="total-amount">$</td>
                                 <?php endif; ?>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                    
                     <input type="hidden" value="" name="product_name">
                      <input type="hidden" value="" id="actual_price" name="product_discounted_price">

                     <button><a href="<?php echo base_url('checkout')?>" class="btn btn__bg d-block">Proceed To Checkout</a></button>
                  </div>
               </div>
            </div>
             <div class="">
                      <td><a href="<?php echo base_url();?>" class="btn btn-warning btn-shop"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                   </div>
         </div>
      </div>
 
      <script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
      <script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>

    <script>
         $(document).ready(function(){
             function minicart(){
   $.ajax({
     url: '<?php echo base_url("cart/minicart");?>',
     type: 'post',     
     success: function(data) {
      $('#minicart').html(data);
    }
  });
 }
minicart();
         $(".cart-remove-item").on('click',function(){
            // alert('a');
        var x = $('.cart-remove-item').length;
        var temp = $(this).parents('tr');
        $.ajax({
      type: "POST",
      url: "<?php echo base_url('cart/remove') ?>",  
      data: {rowid:$(this).data('id')},   
      dataType: "html",     
      success: function(data){
          if(x == 1){
              window.location.reload();
          }else{
             $(temp).remove(); 
          }
                toastr.success('Updated Successfully')
      },
      error: function(data) {
                toastr.error('Something Went Wrong, Please Try Again Later.')
      },
    }); 
    });
         });
    </script>

    <script type="text/javascript">

        function cart_updates(thisobj){
        var qty = $(thisobj).val();
        var rowid = $(thisobj).data('id');
        var id = $(thisobj).data('proid');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url('cart/update')?>",  
          data: {rowid:rowid,qty:qty,id:id},    
          dataType: "html", 
          success: function(cr_data){
            if(cr_data != "false"){
              $("#cartmain").html(cr_data);
            }
            console.log(cr_data);
          },
        }); 

}
</script>

      <script>
         AOS.init();
      </script>
   </body>
</html>