

<div class="dashBoard">
     <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
             
             <a href="<?php echo base_url('dashboard');?>" class="dashboardbtn active">Dashboard</a>
             <a href="<?php echo base_url('accountdetails');?>" class="dashboardbtn">Account Details</a>
             <a href="<?php echo base_url('orderhistory');?>" class="dashboardbtn">Order History</a>
             
             <a href="<?php echo base_url('register/logout');?>" class="dashboardbtn">Log Out</a>
          </div>
          
          
          <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
          
          <h2 class="text-left dashBoardH4">My Dashboard</h2>
          <div class="row">
          
    <div class="col-md-6">
        <div class="box">
            <div class="box-title">
                <h3 class="display-inline-block">Account Details</h3>
                <a href="<?php echo base_url('accountdetails')?>" class="pull-right">Edit <i class="fa fa-edit"></i></a>
            </div>
            <div class="box-content">
                
                   <p class="no-margin-bottom margin-five"><?php echo base_url($customer->customer_name)?$customer->customer_name:''?></p>
                    <p><?php echo base_url($customer->customer_email)?$customer->customer_email:''?></p>
                    <a class="pass-link" href="#">Change Password</a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-title">
                <h3 class="display-inline-block">Order History</h3>
                <a href="<?php echo base_url('orderhistory');?>" class="pull-right">View <i class="fa fa-edit"></i></a>
            </div>
            <div class="box-content">
                
                   <p>Total Number of Orders : <span class="font-weight-700">5</span></p>   
                    
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-title">
                <h3 class="display-inline-block">Select Your Latest Plans</h3>
            </div>
            <div class="box-content">
                
                   <p class="no-margin-bottom margin-five">Lorem Ipsum De amit Lorem Ipsum De amit</p>
                    <p>Lorem Ipsum De amit Lorem Ipsum </p>
                    <a class="pass-link" href="#">Click here for latest plans</a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-title">
                <h3 class="display-inline-block">Log out</h3>
                
            </div>
            <div class="box-content">
                
                   <p class="no-margin-bottom margin-five">Logout from your account to stay secure</p>
                    <p>Log Out Now</p>
                    <a class="pass-link" href="<?php echo base_url('register/logout')?>">Log out</a>
            </div>
        </div>
    </div>
    
    </div>
          </div>
     </div>
</div>
</div>



<script src="js/bootstrap.js"></script>
</body>
</html>
