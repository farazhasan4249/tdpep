
<!--    <div class="inner-banner-sec">
    <div class="container">
        <div class="contact-banner">
            <h4>contact us</h4>
        </div>
    </div>
</div> -->
<div class="inner-banner-sec">
   <div class="container">
      <div class="inner-banner">
         <div class="inner-banner-txt">
            <h4>contact</h4>
         </div>
      </div>
   </div>
</div>

<section>
    <div class="contact-first">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="contact-forum">
                        <form action="<?php echo base_url('contact')?>" method="post">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <h4>get in touch</h4>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="text" id="contact_fname" name="contact_fname" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>First Name</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="text" id="contact_lname" name="contact_lname" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>Last Name</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="text" id="contact_email" name="contact_email" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>Email</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="Password" id="contact_password" name="contact_password" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>Password</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="text" id="contact_subject" name="contact_subject" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>Subject</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <fieldset class="material">
                                        <input type="text" id="contact_phone" name="contact_phone" placeholder="" autocomplete="off" required>
                                        <hr>
                                        <label>Phone</label>
                                    </fieldset>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <textarea id="contact_message" name="contact_message" placeholder="Message"></textarea>
                                    <button>submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-6 col-lg-4">
                    <div class="contact-information">
                        <h4>information</h4>
                        <p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor.Aliquam quam lectus, facilisis auctor.Aliquam quam lectus, facilisis auctor.facilisis auctor.Aliquam quam lectus, facilisis auctor.Aliquam quam lectus, facilisis auctor.</p>
                        <p>Duis sollicitudin sem bibendum nunc euismod, ac varius mauris fermentum. Nulla nec viverra odio, ut tristique quam.</p>
                        <ul>
                            <li><i class="far fa-envelope"></i><a href="#">contact@company.com</a></li>
                            <li><i class="fas fa-phone"></i><a href="#">123-654-789</a></li>
                            <li><i class="fas fa-globe-asia"></i><a href="#">www.yourdomain.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
<script>
    AOS.init();
</script>
</body>

</html>