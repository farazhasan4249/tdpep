<footer>
   <div class="footer-first-sec">
      <div class="container">
         <div class="footer-links-sec">
            <div class="row">
               <div class="col">
                  <div class="footer-list">
                     <ul class="list-group">
                        <li>
                           <h6>Quick Links</h6>
                        </li>
                        <li><a href="<?php echo base_url('home');?>">Home</a></li>
                        <li><a href="<?php echo base_url('terms');?>">Terms & Conditions</a></li>
                        <li><a href="<?php echo base_url('privacypolicy');?>">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url('login')?>">Login</a></li>
                        <li><a href="<?php echo base_url('register')?>">Register</a></li>
                     </ul>
                  </div>
               </div>
               <!--<div class="col">-->
               <!--   <div class="footer-list">-->
               <!--      <ul class="list-group">-->
               <!--         <li>-->
               <!--            <h6>my account</h6>-->
               <!--         </li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">my account</a></li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">order history</a></li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">wish list</a></li>-->
               <!--         <li><a href="<?php echo base_url('newsletter')?>">newsletter</a></li>-->
               <!--         <li><a href="<?php echo base_url('contact')?>">contact us</a></li>-->
               <!--      </ul>-->
               <!--   </div>-->
               <!--</div>-->
               <!--<div class="col">-->
               <!--   <div class="footer-list">-->
               <!--      <ul class="list-group">-->
               <!--         <li>-->
               <!--            <h6>my account</h6>-->
               <!--         </li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">my account</a></li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">order history</a></li>-->
               <!--         <li><a href="<?php echo base_url('login');?>">wish list</a></li>-->
               <!--         <li><a href="<?php echo base_url('newsletter')?>">newsletter</a></li>-->
               <!--         <li><a href="<?php echo base_url('contact')?>">contact us</a></li>-->
               <!--      </ul>-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col">
                  <div class="footer-list">
                     <ul class="list-group">
                        <li >
                           <h6>my account</h6>
                        </li>
                        <li><a href="<?php echo base_url('login');?>">my account</a></li>
                        <li><a href="<?php echo base_url('login');?>">order history</a></li>
                        <li><a href="<?php echo base_url('login');?>">wish list</a></li>
                        <li><a href="<?php echo base_url('newsletter')?>">newsletter</a></li>
                        <li><a href="<?php echo base_url('contact')?>">contact us</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col">
                  <div class="footer-list">
                     <h6>contact list</h6>
                     <p>600, Big market Site Designs
                        custom Boltacusta avenue apt.
                        Mesa,California
                     </p>
                     <a href="mailto:info@customdesign.com">info@customdesign.com</a>
                     <a href="tel:+911234567890">(+91) 12-3456-7890</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="footer-subs-sec">
      <div class="container">
         <div class="row  align-content-center align-items-center">
            <div class="col-md-6">
               <h2>Subscribe for our offer news</h2>
            </div>
            <div class="col-md-6">
               <form action="<?php echo base_url('newsletter')?>" method="post">
               <input type="email" id="newsletter_email" name="newsletter_email" placeholder="Enter your email address">
               <span><button>subscribe</button></span>
            </form>
            </div>
         </div>
      </div>
   </div>
   <div class="footer-downloads-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-4 text-center">
               <p>payment option</p>
               <img src="<?php echo base_url('assets/front/images');?>/payment-img.jpg" alt="payment-img" class="img-fluid" />
            </div>
            <div class="col-md-4 text-center">
               <p>payment option</p> 
               <img src="<?php echo base_url('assets/front/images');?>/downloads-img.jpg" alt="downloads-img" class="img-fluid" />
            </div>
            <div class="col-md-4 text-center">
               <p>social media</p>
               <div class="footer-social-icons">
                  <ul class="list-inline">
                     <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                     <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                     <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                     <li class="list-inline-item"><a href="#"><i class="fa fa-rss"></i></a></li>
                     <li class="list-inline-item"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="footer-copy-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <p>© <?php echo date('Y');?> - TDPEP All Right Reserved&nbsp;</p>
               <div class="terms-links">
                  <ul class="list-inline">
                     <li class="list-inline-item"><a href="<?php echo base_url('terms')?>">terms & conditions</a></li>
                     <li class="list-inline-item"><a href="<?php echo base_url('privacypolicy')?>">privacy policy</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
