<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>TDPEP Naturals</title>
   <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
   <link rel="icon" type="image/png" href="<?php echo base_url('uploads/settings/').$this->favicon;?>">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/owl.carousel.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/aos.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/custom.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/responsive.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css')?>/dashboard.style.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
</head>
<body>
  <?php if($this->session->flashdata('msg') == 1):?>
    <script>toastr.success('<?php echo $this->session->set_flashdata('alert_data')?>');</script>
  <?php elseif($this->session->flashdata('msg') == 2):?>
    <script>toastr.error('<?php echo $this->session->set_flashdata('alert_data')?>');</script>
  <?php endif;?>

   <!-- Back to top button -->
   <a id="button"></a>
   <header>
      <div class="top-bar">
         <div class="container" style="position: relative;">
            <div class="row align-items-center align-content-center">
               <div class="col-md-6">
                  <div class="top-box">
                     <ul class="list-inline">
                        <li class="list-inline-item"><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li class="list-inline-item"><a href="#">English</a></li>
                        <?php $wish_count = count($this->general->get_list('wishlist',array('customer_id'=>$this->session->userdata('customer_id'))));?>
                         <?php if($this->session->userdata("customer_id")): ?>
                        <li class="list-inline-item"><a href="<?php echo base_url('wishlist')?>">Whishlist <span><?php echo $wish_count;?></span></a></li>
                        <?php else :?>
                            <li class="list-inline-item"><a href="<?php echo base_url('wishlist')?>">Whishlist <span>0</span></a></li>
                            <?php endif; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="search-box">
                      <form method="post" action="<?php echo base_url('search/search_button');?>">
                     <input type="text" id="search" name="search" placeholder="What can we help you find today" />
                     <button type="submit"><i class="fa fa-search"></i></button>
                  </form>
                  </div>
               </div>
               <div class="col-md-2 text-right moble-styleing" style="position: absolute;top: -6px;right: 0;">
                 <a href="<?php echo base_url('cart')?>"> <img src="<?php echo base_url('assets/front/images');?>/cart-img.png" alt="cart-img" class="img-fluid"></a>
                  <div class="cart-text">
                     <p>cart <span id="minicart">0</span></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="navig-sec">
         <div class="container">
            <div class="row align-items-center align-content-center">
               <div class="col-md-3  col-9">
                  <a href="<?php echo base_url('home')?>"><img src="<?php echo base_url('uploads/settings/').$this->header_logo;?>" alt="logo" class="img-fluid"></a>
               </div>


               <div class="col-2 d-sm-none d-lg-none d-md-none toogle">
                  <img src="assets\front\images\menu.png">
               </div>



               <div class="col-md-7 no-margin no-padding">
                  <div class="navig">
                     <ul class="list-inline">
                        <?php if (!$this->session->userdata('customer_email')): ?>
                        <li class="list-inline-item"><a href="<?php echo base_url('home')?>">home</a></li>
                        <li class="list-inline-item"><a href="<?php echo base_url('categories')?>">categories</a></li>
                        <li class="list-inline-item"><a href="<?php echo base_url('products')?>">products</a></li>
                        <li class="list-inline-item"><a href="<?php echo base_url('sale')?>">bestseller</a></li>
                        <li class="list-inline-item"><a href="<?php echo base_url('newarrival')?>">new arrival</a></li>
                        <li class="list-inline-item"><a href="<?php echo base_url('contact')?>">contact us</a></li>

                        <?php else: ?>
                         
                         <!-- <li class="list-inline-item"><a href="<?php echo base_url('home')?>">home</a></li>  -->
                       <li class="list-inline-item"><a href="<?php echo base_url('dashboard')?>">Dashboard</a></li>
                       <li class="list-inline-item"><a href="<?php echo base_url('products')?>">products</a></li>
                       <li><a href="<?php echo base_url('register/logout')?>"> LOGOUT <i class="fa fa-sign-out" title="logout"></i> </a></li>

                      <?php endif; ?>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 no-margin no-padding">
                  <div class="navig-account-box">
                     <img src="<?php echo base_url('assets/front/images');?>/account-img.png" alt="account-img" class="img-fluid" />
                     <span>
                        <p>my account</p>
                        <a href="<?php echo base_url('register')?>">register</a><span> & </span><a href="<?php echo base_url('login')?>">login</a>
                     </span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </header>