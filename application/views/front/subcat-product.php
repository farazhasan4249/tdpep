

<body>

  <!-- Back to top button -->
  <a id="button"></a>


  <div class="inner-banner-sec">
   <div class="container">
    <div class="inner-banner">
      <div class="inner-banner-txt">
        <h4>Sub Category</h4>
      </div>
    </div>
  </div>
</div>
<section>
  <div class="best-seller-sec">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
         <div class="sidenav">
          <h4>Product categories</h4>
          <div class="new-list">
            <ul>
             <?php if(isset($category)): foreach($category as $cat): ?>
              <?php $sub_cat = get_list('sub_category',array('category_id' => $cat->category_id));?>
              <!--    <p class="category-name">Bracelets</p> -->
              <li class="toogle_off">
                <button class="dropdown-btn d-flex align-items-center"><a href=" <?php echo base_url('category/').$cat->category_slug;?>"><b><?php echo $cat->category_name; ?> </b><span>(<?php echo get_cat_count($cat->category_id);?>)</span></a><i class="fa fa-plus"></i>
                </button>
                <div class="dropdown-container">
                  <?php if(!empty($sub_cat)): foreach($sub_cat as $row):?> 
                   <a href="<?php echo base_url('subcategory/').$row->sub_category_slug;?>"><?php echo $row->sub_category_name; ?></a>
                 <?php endforeach; endif; ?>
               </div>
             </li>
           <?php endforeach; endif; ?>
         </ul>
       </div>
     </div>
        <!--   <div class="side-nav-second">
            <h4>Filter by Color</h4>
            <ul>
              <li>Gentle Facial Wash <span>(5)</span></li>
              <li>AHAVA Mineral Body <span>(5)</span></li>
              <li>AHAVA Mineral Body <span>(4)</span></li>
            </ul>
          </div>
          <div class="side-nav-second">
            <h4>Filter by size</h4>
            <ul>
              <li>16 <span>(4)</span></li>
              <li>17 <span>(4)</span></li>
              <li>18 <span>(4)</span></li>
              <li>19 <span>(4)</span></li>
              <li>20 <span>(4)</span></li>
            </ul>
          </div> -->
          <div class="side-nav-third">
            <h4>best selling product</h4>
            <div class="row">
              <?php if(isset($newarrival) && !empty($newarrival)) { ?>
                <?php foreach($newarrival as $new) { ?>
                  <div class="col-sm-5 no_span">
                    <img src="<?php echo base_url('uploads/settings/').$new->newarrival_image;?>" class="img-responsive">
                  </div>
                  <div class="col-sm-7">
                    <h6><?php echo base_url($new->newarrival_name)?$new->newarrival_name:''?></h6>
                    <h5>$450</h5>
                  </div>
                <?php }} ?>
              </div>

            <!-- <div class="row">
              <div class="col-sm-5 no_span">
                <img src="<?php echo base_url('assets/front/images');?>/2.jpg" class="img-responsive">
              </div>
              <div class="col-sm-7">
                <h6>Arced Custom Ring Design 20k Sterling Gold</h6>
                <h5>599.00$</h5>
              </div>
            </div>
           
            <div class="row">
              <div class="col-sm-5 no_span">
                <img src="<?php echo base_url('assets/front/images');?>/3.jpg" class="img-responsive">
              </div>
              <div class="col-sm-7">
                <h6>Rhombus Diamond Ring 18k White Gold</h6>
                <h3>128.00$</h3><span>99.00$</span>
              </div>
            </div> -->
          </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <?php if(isset($product) && !empty($product)) { ?>
              <?php foreach($product as $pro) { ?>
                <div class="col-md-4">
                  <div class="product-grid">
                    <div class="product-image">
                      <a href="#">
                        <img class="pic-1" src="<?php echo ('').$pro->product_image;?>" alt="deal-pro-img-1" class="img-fluid" />
                      </a>
                      <ul class="social">
                        <li><button type="submit" class="add_to_wish" data-id="<?php echo $pro->product_id;?>"> <i class="fa fa-shopping-bag"></i></button></li>
                        <li><a href="<?php echo base_url('product_detail/').$pro->product_slug;?>"><i class="fa fa-shopping-cart"></i></a></li>
                      </ul>
                      <span class="product-new-label">Sale</span>
                      <span class="product-discount-label">20%</span>
                    </div>
                    <ul class="rating">
                      <li class="fa fa-star"></li>
                      <li class="fa fa-star"></li>
                      <li class="fa fa-star"></li>
                      <li class="fa fa-star"></li>
                      <li class="fa fa-star disable"></li>
                    </ul>
                    <div class="product-content">
                      <h3 class="title"><a href="#"><?php echo base_url($pro->product_name)?$pro->product_name:''?></a></h3>
                      <div class="price">$<?php echo base_url($pro->product_discounted_price)?$pro->product_discounted_price:''?>
                      <span>$<?php echo base_url($pro->product_reg_price)?$pro->product_reg_price:''?></span>
                    </div>
                    <button class="add-to-cart cart_form" id="cart_form"><a href="<?php echo base_url('product_detail/').$pro->product_slug;?>">+ Add To Cart</a></button>
                  </div>
                </div>
              </div>
            <?php }} ?>
          </div>
          <div class="paginations">
            <?php echo $this->pagination->create_links();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>

<script>
  // alert('a');
  $(document).ready(function(){

    $(".add_to_wish").on('click',function(){
      // alert('a');
      $.ajax({
        url: '<?php echo base_url('wishlist/wish');?>',
        type: 'post',
        data: {product_id:$(this).data("id"),
      },
      success: function(data) {

        if(data == 1){
          toastr.success('Added To Wishlist.');
          location.reload();
        }else if(data == 2){
          toastr.error('Something Went Wrong, Please Try Again Later.')
        }
        else if(data == 3){
          toastr.error('Already in your wishlist')
        }
        else if(data == 4){
          toastr.error('Login First Please')
        }

      }
    });
    }); 
  });
</script>

<script>
 function carttotal(){  
  var id = $(this).parents('tr').data('id');    
  $.ajax({
    url: '<?php echo base_url('cart/update')?>',
    type: 'post',   
    data: {id: id}, 
    success: function(data) {
      $('.carttotal').html(data);

    }
  });
}
</script>
<script>
  AOS.init();
</script>
</body>

</html>