   
   <div class="banner-sec">
      <div class="container">
         <div class="banner-image">
            <img src="<?php echo base_url('assets/front/images');?>/banner-img.png" alt="banner-img" class="img-fluid"/>
            <div class="banner-txt">
               <p>Save Now Beauty of Life</p>
               <h3>treatments For all</h3>
               <h3>Spa Cosmetics</h3>
               <button class="btn-theme">shop collection now !</button>
            </div>
         </div>
      </div>
   </div>
   <section class="why-choose-sec">
      <div class="container">
         <div class="row why-choose-bg-cl h-100 align-items-center align-content-center no-margin">
            <div class="col-md-3">
               <h4>why choose us ?</h4>
            </div>
            <div class="col-md-3">
               <div class="why-choose-box">
                  <img src="<?php echo base_url('assets/front/images');?>/choose-img-1.png" alt="choose-img-1" class="img-fluid" />
                  <span>
                     <p>On time delivery</p>
                     <p>15% back if not able</p>
                  </span>
               </div>
            </div>
            <div class="col-md-3">
               <div class="why-choose-box">
                  <img src="<?php echo base_url('assets/front/images');?>/choose-img-2.png" alt="choose-img-2" class="img-fluid" />
                  <span>
                     <p>Free delivery</p>
                     <p>Order over $ 200</p>
                  </span>
               </div>
            </div>
            <div class="col-md-3">
               <div class="why-choose-box">
                  <img src="<?php echo base_url('assets/front/images');?>/choose-img-3.png" alt="choose-img-2" class="img-fluid" />
                  <span>
                     <p>Quality assurance</p>
                     <p>You can trust us</p>
                  </span>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="stock-deals">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h4>save more with stackable deals</h4>
            </div>
         </div>
         <div class="row">
            <?php if(isset($newarrival) && !empty($newarrival)) { ?>
            <?php foreach($newarrival as $new) { ?>
            <div class="col-md-2">
               <div class="deal-pr-box">
                  <p class="deal-pr-box-p"><?php echo base_url($new->newarrival_name)?$new->newarrival_name:''?></p>
                  <img src="<?php echo base_url('uploads/settings/').$new->newarrival_image;?>" alt="deal-pro-img" class="img-fluid" />
                  <div class="cart-box">
                     <a href="<?php echo base_url('newarrival/').$new->newarrival_id?>"><img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid"></a>
                  </div>
               </div>
            </div>
         <?php }} ?>
         </div>
      </div>
   </section>
   <section>
      <div class="container">

 <div class="slider">           
<div class="owl-carousel owl-theme">
    <?php if(isset($home) && !empty($home)) { ?>
               <?php foreach($home as $slide) { ?>
              <div class="item">
            <div class="col-md-12">
            
               
                  <div class="row">

                        <div class="col-md-5 text-center">
                           <img src="<?php echo base_url('uploads/settings/').$slide->home_slider_image;?>" alt="hot-deal-img" class="img-fluid" />
                        </div>


                        <div class="col-md-6 text-center">
                            <div class="hot-deal-corner">
                            <div class="hot-deal-box">
                              <h3>hot <span>deal</span></h3>
                              <p><?= $slide->home_slider_text;?></p>
                           </div>
                           <div class="timer">
                              <div class="time-des">
                                 <p class="day"></p>
                                 <p>days</p>
                              </div>
                              <div class="colon">:</div>
                              <div class="time-des">
                                 <p class="hour"></p>
                                 <p>hours</p>
                              </div>
                              <div class="colon">:</div>
                              <div class="time-des">
                                 <p class="minute"></p>
                                 <p>min</p>
                              </div>
                              <div class="colon">:</div>
                              <div class="time-des">
                                 <p class="second"></p>
                                 <p>sec</p>
                              </div>
                           </div>
                           <div class="corner-1">
                               <img src="<?php echo base_url('assets/front/images');?>/hot-deal-corner-1.png" alt="corner-1" class="img-fluid" />
                           </div>
                            <div class="corner-2">
                               <img src="<?php echo base_url('assets/front/images');?>/hot-deal-corner-2.png" alt="corner-2" class="img-fluid" />
                           </div>
                            </div>
                           <button class="btn-theme">shop collection now !</button>
                        </div>
                         
                     </div>
            
                  <div class="hot-deal-border">
                      <img src="<?php echo base_url('assets/front/images');?>/hot-deal-border.png" alt="hot-deal-border" class="img-fluid" />
                  </div>
           </div>
           </div>
            <?php }} ?>
        </div>
    </div>
         </div>
  
</section>
<!--<section class="shop-by-depart">-->
<!--   <div class="container">-->
<!--      <div class="row">-->
<!--         <div class="col-md-12">-->
<!--            <h5>shop by department</h5>-->
<!--         </div>-->
<!--      </div>-->
<!--      <div class="row">-->
<!--         <div class="col-md-4">-->
<!--            <div class="shop-depart-box-l">-->
<!--               <div class="shop-depart-inner">-->
<!--                  <p>Nunc tortor urna Faucibus non</p>-->
<!--                  <span><a href="#"><img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid" /></a></span>-->
<!--               </div>-->
<!--               <img src="<?php echo base_url('assets/front/images');?>/shop-img-1.png" alt="shop-img-1" class="img-fluid shop-depart-img" />-->
<!--            </div>-->
<!--         </div>-->
<!--         <div class="col-md-8">-->
<!--            <div class="row">-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor Faucibus nonips</p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-2.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor-->
<!--                           Fausicvt-->
<!--                           jiopi-->
<!--                        </p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-3.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor-->
<!--                           Tumisg-->
<!--                           lipd-->
<!--                        </p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-4.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor-->
<!--                           Fausicov-->
<!--                           noipes-->
<!--                        </p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-5.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor-->
<!--                           Gertpo limtuy-->
<!--                           seomt-->
<!--                        </p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-6.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--               <div class="col-md-4">-->
<!--                  <div class="shop-depart-box-r">-->
<!--                     <div class="shop-depart-inner-r">-->
<!--                        <p>Nunc tortor-->
<!--                           lipsum ipmrte-->
<!--                           neoemw-->
<!--                        </p>-->
<!--                        <img src="<?php echo base_url('assets/front/images');?>/shop-img-7.png" alt="shop-img-2" class="img-fluid shop-depart-img-r" />-->
<!--                     </div>-->
<!--                     <img src="<?php echo base_url('assets/front/images');?>/cart-icon.png" alt="cart-icon" class="img-fluid shop-depart-inner-r-icon" />-->
<!--                  </div>-->
<!--               </div>-->
<!--            </div>-->
<!--         </div>-->
<!--      </div>-->
<!--      <div class="row mt-40">-->
<!--         <div class="col-md-12 text-center">-->
<!--            <button class="btn-theme">view all collection</button>-->
<!--         </div>-->
<!--      </div>-->
<!--   </div>-->
<!--</section>-->
<section class="blogs-sec">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h5>latest news & events</h5>
         </div>
      </div>
      <div class="row">
         <div class="col-md-4">
            <div class="blog-box">
               <div class="blog-box-img">
                  <img src="<?php echo base_url('assets/front/images');?>/blog-img-3.jpg" alt="blog-img-1" class="img-fluid" />
               </div>
               <p>Interdum et mal</p>
               <p>Esuada fames ac ante ipsum primis in faucibus. In hac habitasse platea dictumst.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="blog-box">
               <div class="blog-box-img">
                  <img src="<?php echo base_url('assets/front/images');?>/blog-img-2.jpg" alt="blog-img-1" class="img-fluid" />
               </div>
               <p>Interdum et mal</p>
               <p>Esuada fames ac ante ipsum primis in faucibus. In hac habitasse platea dictumst.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="blog-box">
               <div class="blog-box-img">
                  <img src="<?php echo base_url('assets/front/images');?>/blog-img-1.jpg" alt="blog-img-1" class="img-fluid" />
               </div>
               <p>Interdum et mal</p>
               <p>Esuada fames ac ante ipsum primis in faucibus. In hac habitasse platea dictumst.</p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="logo-sec">
   <div class="container">
      <div class="row">
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
         <div class="col-md-2">
            <div class="logo-box">
               <img src="<?php echo base_url('assets/front/images');?>/logo-img-1.png" alt="logo-img" class="img-fluid" />
            </div>
         </div>
      </div>
   </div>
</section>
<script src="<?php echo base_url('assets/front/js')?>/jquery.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/aos.min.js"></script>
<script src="<?php echo base_url('assets/front/js')?>/custom.js"></script>
<script>
   AOS.init();
</script>
</body>
</html>