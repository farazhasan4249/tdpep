<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Category</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/category/add')?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
                <div class="form-group">
                  <label>Category Name:</label>
                  <input type="text" class="form-control" id="category_name" name="category_name" required>
                  <?php echo form_error('category_name'); ?>
                </div>
                
                <div class="form-group">
                  <label>Category slug:</label>
                  <input type="text" class="form-control" id="category_slug" name="category_slug" required>
                  <?php echo form_error('category_slug'); ?>
                </div>
                
                <div class="form-group">
                    <label>Category Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="category_image" name="category_image">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('category_image'); ?>
                </div>
              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>