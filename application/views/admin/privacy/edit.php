
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Privacy Page</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/privacy/edit');?>" method="post" enctype="multipart/form-data">       
               <div class="box-body">              
                <div class="form-group">
                  <label>Privacy heading</label>
                  <input type="name" class="form-control" id="privacy_heading" name="privacy_heading" value="<?php echo !empty($record->privacy_heading)?$record->privacy_heading:''?>" required>
                  <?php echo form_error('privacy_heading'); ?>
                </div> 

                <div class="form-group">
                  <label>Privacy Sub Heading</label>
                  <input type="name" class="form-control" id="privacy_subheading" name="privacy_subheading" value="<?php echo !empty($record->privacy_subheading)?$record->privacy_subheading:''?>" required>
                  <?php echo form_error('privacy_subheading'); ?>
                </div>              
                <div class="form-group">
                  <label>Privacy Content</label>
                  <textarea class="editor form-control" rows="3" id="privacy_content" name="privacy_content" required><?php echo !empty($record->privacy_content)?$record->privacy_content:''?></textarea>
                  <?php echo form_error('privacy_content'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>