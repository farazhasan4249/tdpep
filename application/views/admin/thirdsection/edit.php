<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/thirdsec/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Third Section Heading</label>
                  <input type="name" class="form-control" id="third_section_heading" name="third_section_heading" value="<?php echo !empty($record->third_section_heading)?$record->third_section_heading:''?>" required>
                  <?php echo form_error('third_section_heading'); ?>
                </div>              
      
                <div class="form-group">
                  <label>Third Section Button Text</label>
                  <textarea class="editor form-control" rows="3" id="third_section_butn_text" name="third_section_butn_text" required><?php echo !empty($record->third_section_butn_text)?$record->third_section_butn_text:''?></textarea>
                  <?php echo form_error('third_section_butn_text'); ?>
                </div>
              </div>
                <div class="form-group">
                <label>Third Section Video</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->third_section_video)?base_url('uploads/videos/').$record->third_section_video:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="third_section_video" name="third_section_video">
                      <input type="text" id="third_section_video" name="third_section_video" value="<?php echo !empty($record->third_section_video)?$record->third_section_video:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('third_section_video'); ?>
              </div>  

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".video-upload").on("change","input[type=file]",function() {      
            $(this).parents('.pro_videos').children('.col-md-4').children('.video-up').html('<img class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
          });
      
         $(".video-upload").on("click",".video-up i.fa.fa-times",function() {  
            $(this).parents('.video-up').html('');
            $(this).parents('.pro_videos').children('input[type=file]').val('');
   
   });
  });
</script>