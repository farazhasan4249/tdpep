<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Newsletter Inquiry</h3>

              <p>View Subscribed Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url('admin/newsletter/list');?>" class="small-box-footer">View Inquiries <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>Contact Inquiry</h3>

              <p>View Contact Information</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url('admin/contact/list');?>" class="small-box-footer">View Inquiries <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
        <!--   <div class="small-box bg-green">
            <div class="inner">
              <h3>Technology<sup style="font-size: 20px"></sup></h3>

              <p>Technologies</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo base_url('admin/technology/edit');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div> -->
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
        <!--   <div class="small-box bg-yellow">
            <div class="inner">
              <h3>About</h3>

              <p>about us</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('admin/about_section/edit');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div> -->
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
         <!--  <div class="small-box bg-red">
            <div class="inner">
              <h3>Contact</h3>

              <p>contact us</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?php echo base_url('admin/contact_page/edit');?>" class="small-box-footer">Address<i class="fa fa-arrow-circle-right"></i></a>
          </div> -->
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->

      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  