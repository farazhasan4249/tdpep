
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Terms Page</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/terms/edit');?>" method="post" enctype="multipart/form-data">       
               <div class="box-body">              
                <div class="form-group">
                  <label>Terms Heading</label>
                  <input type="name" class="form-control" id="terms_heading" name="terms_heading" value="<?php echo !empty($record->terms_heading)?$record->terms_heading:''?>" required>
                  <?php echo form_error('terms_heading'); ?>
                </div> 

                <div class="form-group">
                  <label>Terms Sub Heading</label>
                  <input type="name" class="form-control" id="terms_subheading" name="terms_subheading" value="<?php echo !empty($record->terms_subheading)?$record->terms_subheading:''?>" required>
                  <?php echo form_error('terms_subheading'); ?>
                </div>              
                <div class="form-group">
                  <label>Terms Content</label>
                  <textarea class="editor form-control" rows="3" id="terms_content" name="terms_content" required><?php echo !empty($record->terms_content)?$record->terms_content:''?></textarea>
                  <?php echo form_error('terms_content'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>