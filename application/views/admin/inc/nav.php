 <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo !empty($this->session->userdata('master_admin_image'))?base_url('uploads/admin/').$this->session->userdata('master_admin_image'):base_url('admin/assets/img/placeholder.jpg');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo !empty($this->session->userdata('master_admin_name'))?$this->session->userdata('master_admin_name'):'Master Admin';?></p>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('admin/login');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="javascript:;">
            <i class="fa fa-wrench"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/settings/general');?>"><i class="fa fa-circle-o"></i>General</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span>Site Content</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/home');?>"><i class="fa fa-circle-o"></i>Home Section</a></li>
            <li><a href="<?php echo base_url('admin/terms/edit');?>"><i class="fa fa-circle-o"></i>Terms</a></li>
            <li><a href="<?php echo base_url('admin/privacy/edit');?>"><i class="fa fa-circle-o"></i>Privacy</a></li>
            
            
        
            <!-- <li><a href="<?php echo base_url('admin/technology/edit');?>"><i class="fa fa-circle-o"></i>Technology</a></li>
            <li><a href="<?php echo base_url('admin/thirdsec/edit');?>"><i class="fa fa-circle-o"></i>Third Section</a></li>
            <li><a href="<?php echo base_url('admin/About-section/edit');?>"><i class="fa fa-circle-o"></i>About Section</a></li> --> 
            <!-- <li><a href="<?php echo base_url('admin/contact/edit');?>"><i class="fa fa-circle-o"></i>Contact Page</a></li> -->
           <!--  <li><a href="<?php echo base_url('admin/social');?>"><i class="fa fa-circle-o"></i>Social Link</a></li> -->
          </ul>
        </li>

          <li class="treeview">
          <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span>Inventory Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            
            
         <li><a href="<?php echo base_url('admin/category');?>"><i class="fa fa-circle-o"></i>Categories Settings</a></li>
         <li><a href="<?php echo base_url('admin/subcategory');?>"><i class="fa fa-circle-o"></i>Sub Categories Settings</a></li>
          <li><a href="<?php echo base_url('admin/newarrival');?>"><i class="fa fa-circle-o"></i>New Arrival Settings</a></li>
          <li><a href="<?php echo base_url('admin/products');?>"><i class="fa fa-circle-o"></i>Product Settings</a></li>
            <!-- <li><a href="<?php echo base_url('admin/technology/edit');?>"><i class="fa fa-circle-o"></i>Technology</a></li>
            <li><a href="<?php echo base_url('admin/thirdsec/edit');?>"><i class="fa fa-circle-o"></i>Third Section</a></li>
            <li><a href="<?php echo base_url('admin/About-section/edit');?>"><i class="fa fa-circle-o"></i>About Section</a></li> --> 
            <!-- <li><a href="<?php echo base_url('admin/contact/edit');?>"><i class="fa fa-circle-o"></i>Contact Page</a></li> -->
           <!--  <li><a href="<?php echo base_url('admin/social');?>"><i class="fa fa-circle-o"></i>Social Link</a></li> -->
          </ul>
        </li>
        
          
        </li>
        <li class="treeview">
          <a href="javascript:;">
            <i class="fa fa-picture-o"></i>
            <span>Inquiries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          
        <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/newsletter/list');?>"><i class="fa fa-circle-o"></i>Newsletter</a></li>
            <li><a href="<?php echo base_url('admin/contact/list');?>"><i class="fa fa-circle-o"></i>Contact Inquiry</a></li>
          </ul>
        </li>
        
    </section>
  </aside>
  <?php if(isset($output)):?>
  <div class="content-wrapper">    
    <?php echo $output;?>
  </div>
  <?php endif;?>