<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Products</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">

               <div class="form-group">
                  <span class="col-md-2 view_label">Product Category:</span>
                  <span class="col-md-10 view_details"><?php echo get_name_by_id('category',$record->category_id);?></span>
              </div>

               <div class="form-group">
                  <span class="col-md-2 view_label">Product SubCategory</span>
                  <span class="col-md-10 view_details"><?php echo get_name_by_id('sub_category',$record->sub_category_id);?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Product Name:</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_name;?></span>
              </div>
            
            <div class="form-group">
                  <span class="col-md-2 view_label">Product Slug:</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_slug;?></span>
              </div>

                <div class="form-group">
                  <span class="col-md-2 view_label">Product Image:</span>
                  <span class="col-md-10 view_details"><img style="height:100px" src="<?php echo base_url('uploads/settings/').$record->product_image;?>"></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Product Regular Price:</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_reg_price;?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Product Discounted Price:</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_discounted_price;?></span>
              </div>
              
               <div class="form-group">
                  <span class="col-md-2 view_label">Product Short Description</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_short_desc;?></span>
              </div>
       
               <div class="form-group">
                  <span class="col-md-2 view_label">Product Long Description</span>
                  <span class="col-md-10 view_details"><?php echo $record->product_long_desc;?></span>
              </div>


            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
