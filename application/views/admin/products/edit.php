
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Product</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/products/edit');?>" method="post" enctype="multipart/form-data">     

               <div class="box-body">  


               <div class="form-group">
                <label class="col-md-2">
                  Category:
                </label>
                <div class="col-lg-10">
                  <select class="form-control" id="category_id" name="category_id" data-validation="required">
                    <?php $category = get_list('category');?>
                    <?php if(!empty($category)): foreach($category as $cat):?>
                      <option value="<?php echo $cat->category_id;?>"  <?php echo ($cat->category_id == $record->category_id ?'selected':'')?>><?php echo $cat->category_name;?></option>
                    <?php endforeach; endif;?>
                  </select>         
                </div>
              </div>  

              <div class="form-group">
                <div class="col-lg-2">
                 <label>Sub Category: </label>
               </div>
               <div class="col-lg-10">
                <select class="form-control" id="sub_category_id" name="sub_category_id">
                  <?php $category = get_list('sub_category',array('category_id'=>$record->category_id));?>
                  <option value="<?php echo $record->sub_category_id;?>"><?php echo get_name_by_id('sub_category',$record->sub_category_id);?></option>
                  <?php if(!empty($category)): foreach($category as $subcat):?>
                    <option value="<?php echo $subcat->sub_category_id;?>"><?php echo $subcat->sub_category_name;?></option>
                  <?php endforeach; endif;?>
                </select>         
              </div>
            </div>  



                <div class="form-group">
                <label>Product image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->product_image)?base_url('uploads/settings/').$record->product_image:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="product_image" name="product_image">
                      <input type="text" id="product_image" name="product_image" value="<?php echo !empty($record->product_image)?$record->product_image:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('product_image'); ?>
              </div>   


        <div class="clearfix"></div>

              
               <div class="form-group">
                  <label>Product Name</label>
                  <input type="name" class="form-control" id="product_name" name="product_name" value="<?php echo !empty($record->product_name)?$record->product_name:''?>" required>
                  <?php echo form_error('product_name'); ?>
                </div> 

                 <div class="form-group">
                  <label>Product Slug</label>
                  <input type="name" class="form-control" id="product_slug" name="product_slug" value="<?php echo !empty($record->product_slug)?$record->product_slug:''?>" required>
                  <?php echo form_error('product_slug'); ?>
                </div> 

                 <div class="form-group">
                  <label>Product Regular Price</label>
                  <input type="name" class="form-control" id="product_reg_price" name="product_reg_price" value="<?php echo !empty($record->product_reg_price)?$record->product_reg_price:''?>" required>
                  <?php echo form_error('product_reg_price'); ?>
                </div> 

                 <div class="form-group">
                  <label>Product Discounted Price</label>
                  <input type="name" class="form-control" id="product_discounted_price" name="product_discounted_price" value="<?php echo !empty($record->product_discounted_price)?$record->product_discounted_price:''?>" required>
                  <?php echo form_error('product_discounted_price'); ?>
                </div> 

                <div class="form-group">
                  <label>Product Short Description</label>
                  <textarea class="editor form-control" rows="3" id="product_short_desc" name="product_short_desc" required><?php echo !empty($record->product_short_desc)?$record->product_short_desc:''?></textarea>
                  <?php echo form_error('product_short_desc'); ?>
                </div>

                <div class="form-group">
                  <label>Product Long Description</label>
                  <textarea class="editor form-control" rows="3" id="product_long_desc" name="product_long_desc" required><?php echo !empty($record->product_long_desc)?$record->product_long_desc:''?></textarea>
                  <?php echo form_error('product_long_desc'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>