
<script>

$(document).ready(function (){
  $("#category_id").on("change",function() {   
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('admin/Products/get_dropdown');?>",  
      data: {
          get_from: 'sub_category',
          get_where: 'category',
          id: $(this).val()
        },      
       dataType: "html",     
      success: function(data){
        $('#sub_category_id').html(data); 
      },
      error: function(data) {
        console.log(data);
      }
    });   
  });
});
</script>

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Product</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/products/add')?>" method="post" enctype="multipart/form-data"> 
             
             <div class="box-body">
             <div class="form-group">
              <div class="col-lg-2">
               <label> Category: </label>
             </div>
             <div class="col-lg-10">
              <select class="form-control" id="category_id" name="category_id" data-validation="required">
                <?php $category = get_list('category');?>
                <option value="0">Please Select</option>
                <?php if(!empty($category)): foreach($category as $cat):?>
                  <option value="<?php echo $cat->category_id;?>"><?php echo $cat->category_name;?></option>
                <?php endforeach; endif;?>
              </select>         
            </div>
          </div> 

           <div class="form-group">
              <div class="col-lg-2">
               <label>Sub Category: </label>
             </div>
             <div class="col-lg-10">
              <select class="form-control" id="sub_category_id" name="sub_category_id">
                <?php $category = get_list('sub_category');?>
                <option value="0">Please Select</option>
                <?php if(!empty($category)): foreach($category as $subcat):?>
                  <option value="<?php echo $subcat->sub_category_id;?>"><?php echo $subcat->sub_category_name;?></option>
                <?php endforeach; endif;?>
              </select>         
            </div>
          </div>  


              
                <div class="form-group">
                  <label>Product Name:</label>
                  <input type="text" class="form-control" id="product_name" name="product_name" required>
                  <?php echo form_error('product_name'); ?>
                </div>
                
                
                <div class="form-group">
                    <label>Product Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="product_image" name="product_image">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('product_image'); ?>
                </div>
              </div>

            <!-- <div class="form-group">
            <div class="col-lg-2">
              <label>Make This Featured:</label>
            </div>
            <div class="col-lg-10">
              <div id="radioBtn" class="btn-group">
                <a class="btn btn-primary btn-sm active" data-toggle="product_featured" data-title="1">YES</a>
                <a class="btn btn-primary btn-sm notActive" data-toggle="product_featured" data-title="0">NO</a>
              </div>
              <input type="hidden" name="product_featured" id="product_featured">                 
            </div>
          </div>  -->  

            <div class="box-body">
                <div class="form-group">
                  <label>Product Regular Price:</label>
                  <input type="text" class="form-control" id="product_reg_price" name="product_reg_price" required>
                  <?php echo form_error('product_reg_price'); ?>
                </div>

                 <div class="box-body">
                <div class="form-group">
                  <label>Product Slug:</label>
                  <input type="text" class="form-control" id="product_slug" name="product_slug" required>
                  <?php echo form_error('product_slug'); ?>
                </div>

                <div class="box-body">
                <div class="form-group">
                  <label>Product Discounted Price:</label>
                  <input type="text" class="form-control" id="product_discounted_price" name="product_discounted_price" required>
                  <?php echo form_error('product_discounted_price'); ?>
                </div>

                <div class="form-group">
                  <label>Product short Description</label>
                  <textarea class="editor form-control" rows="3" id="product_short_desc" name="product_short_desc" required><?php echo !empty($record->product_short_desc)?$record->product_short_desc:''?></textarea>
                  <?php echo form_error('product_short_desc'); ?>
                </div>

                <div class="form-group">
                  <label>Product Long Description</label>
                  <textarea class="editor form-control" rows="3" id="product_long_desc" name="product_long_desc" required><?php echo !empty($record->product_long_desc)?$record->product_long_desc:''?></textarea>
                  <?php echo form_error('product_long_desc'); ?>
                </div>


              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
