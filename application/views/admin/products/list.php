<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">   
              <div class="tddts">
                <a href="<?php echo site_url('admin/products/add');?>" class="add-btn">Add New</a>
              </div>            
            </div>  
            <div class="box-body">
              <table id="datatable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Product Category</th>
                  <th>Product Sub-Category</th>
                  <th>Product Name</th>
                  <th>Product Image</th>
                  <th>Product Featured Status</th>
                  <th>Product Regular Price</th>
                  <th>Product Discounted Price</th>
                  <th>Product Long Description</th>
                  <th>Product Short Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>            
                <?php $i=1; if(!empty($records)): foreach($records as $record):?>    
                <tr>
                  <td><?php echo $i;?></td>
                   <td><?php echo get_name_by_id('category',$record->category_id);?></td> 
                   <td><?php echo get_name_by_id('sub_category',$record->sub_category_id);?></td> 
                   <td><?php echo !empty($record->product_name)?limit_text('50',$record->product_name):'';?></td>
                  <td><?php echo !empty($record->product_image)?limit_text('50',$record->product_image):'';?></td>
                  <td><?php if($record->product_featured == 1){ echo "Yes";}else{echo "No";};?></td>
                  <td><?php echo !empty($record->product_reg_price)?limit_text('50',$record->product_reg_price):'';?></td>
                  <td><?php echo !empty($record->product_discounted_price)?limit_text('50',$record->product_discounted_price):'';?></td>
                  <td><?php echo !empty($record->product_short_desc)?limit_text('50',$record->product_short_desc):'';?></td>
                  <td><?php echo !empty($record->product_long_desc)?limit_text('50',$record->product_long_desc):'';?></td>
                  <td>
                    <a href="<?php echo !empty($record->product_id)?base_url('admin/products/edit/').$record->product_id:'';?>"><span class="edit_icon"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                    <a href="<?php echo !empty($record->product_id)?base_url('admin/products/view/').$record->product_id:'';?>"><span class="view_icon"><i class="fa fa-eye" aria-hidden="true"></i></span></a>
                    <a href="<?php echo !empty($record->product_id)?base_url('admin/products/delete/').$record->product_id:'';?>"><span class="delete_icon"><i class="fa fa-trash" aria-hidden="true"></i></span></a>
                  </td>
                </tr>
                <?php $i++; endforeach;?>  
                <?php else:?>
                <tr>
                  <td>No Record Found</td>
                </tr>
                <?php endif;?>
                </tbody>
              </table>
            </div>
         </div>   
      </div>
    </div>
  </section>

</div>
