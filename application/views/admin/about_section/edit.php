
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add About Page</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/About_section/edit');?>" method="post" enctype="multipart/form-data">       
               <div class="box-body">              
                <div class="form-group">
                  <label>About Sec Heading</label>
                  <input type="name" class="form-control" id="about_section_heading" name="about_section_heading" value="<?php echo !empty($record->about_section_heading)?$record->about_section_heading:''?>" required>
                  <?php echo form_error('about_section_heading'); ?>
                </div>              
                <div class="form-group">
                  <label>About Content</label>
                  <input type="name" class="form-control" id="about_section_heading" name="about_section_text" value="<?php echo !empty($record->about_section_text)?$record->about_section_text:''?>" required>
                  <?php echo form_error('about_section_text'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>