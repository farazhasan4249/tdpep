
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Sub Category</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/subcategory/edit');?>" method="post" enctype="multipart/form-data">     

               <div class="box-body">              
                <div class="form-group">
                <label>Sub Category image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->sub_category_image)?base_url('uploads/settings/').$record->sub_category_image:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="sub_category_image" name="sub_category_image">
                      <input type="text" id="sub_category_image" name="sub_category_image" value="<?php echo !empty($record->sub_category_image)?$record->sub_category_image:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('sub_category_image'); ?>
              </div>   

              
               <div class="form-group">
                  <label>Sub Category Name</label>
                  <input type="name" class="form-control" id="sub_category_name" name="sub_category_name" value="<?php echo !empty($record->sub_category_name)?$record->sub_category_name:''?>" required>
                  <?php echo form_error('sub_category_name'); ?>
                </div> 

                 <div class="form-group">
                  <label>Sub Category Slug</label>
                  <input type="name" class="form-control" id="sub_category_slug" name="sub_category_slug" value="<?php echo !empty($record->sub_category_slug)?$record->sub_category_slug:''?>" required>
                  <?php echo form_error('sub_category_slug'); ?>
                </div>

                 <div class="form-group">
                  <label>Sub Category Text</label>
                  <textarea class="editor form-control" rows="3" id="sub_category_text" name="sub_category_text" required><?php echo !empty($record->sub_category_text)?$record->sub_category_text:''?></textarea>
                  <?php echo form_error('sub_category_text'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>