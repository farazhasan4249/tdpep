<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View Sub Categories</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label">SubCategory Name:</span>
                  <span class="col-md-10 view_details"><?php echo $record->sub_category_name;?></span>
              </div>
            
            <div class="form-group">
                  <span class="col-md-2 view_label">SubCategory Text:</span>
                  <span class="col-md-10 view_details"><?php echo $record->sub_category_text;?></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">SubCategory Slug:</span>
                  <span class="col-md-10 view_details"><?php echo $record->sub_category_slug;?></span>
              </div>
              
              <div class="form-group">
                  <span class="col-md-2 view_label">SubCategory Image:</span>
                  <span class="col-md-10 view_details"><img style="height:100px" src="<?php echo base_url('uploads/settings/').$record->sub_category_image;?>"></span>
              </div>
            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
