<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add SubCategory</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/subcategory/add')?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
              
               <div class="col-md-12 col-sm-12">
                <div class="form-group">
                <div class="col-lg-2 control-label">
                  <label> Category: </label>
                </div>
                <div class="col-lg-10">
                  <select class="form-control" id="category_id" name="category_id" data-validation="required">
                    <?php $category = get_list('category');?>
                    <?php if(!empty($category)): foreach($category as $cat):?>
                      <option value="<?php echo $cat->category_id;?>"><?php echo $cat->category_name;?></option>
                    <?php endforeach; endif;?>
                  </select>         
                </div>
              </div>
              </div> 

                <div class="form-group">
                  <label>Sub Category Name:</label>
                  <input type="text" class="form-control" id="sub_category_name" name="sub_category_name" required>
                  <?php echo form_error('sub_category_name'); ?>
                </div>
                
                <div class="form-group">
                  <label>Sub Category Slug:</label>
                  <input type="text" class="form-control" id="sub_category_slug" name="sub_category_slug" required>
                  <?php echo form_error('sub_category_slug'); ?>
                </div>
                
                <div class="form-group">
                    <label>Sub Category Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="sub_category_image" name="sub_category_image">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('sub_category_image'); ?>
                </div>
              </div>

               <div class="form-group">
                  <label>Sub Category Text</label>
                  <textarea class="editor form-control" rows="3" id="sub_category_text" name="sub_category_text" required><?php echo !empty($record->sub_category_text)?$record->sub_category_text:''?></textarea>
                  <?php echo form_error('sub_category_text'); ?>
                </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>