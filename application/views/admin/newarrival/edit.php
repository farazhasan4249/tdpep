
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add New Product</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/newarrival/edit');?>" method="post" enctype="multipart/form-data">     

               <div class="box-body">              
                <div class="form-group">
                <label>New Product image</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->newarrival_image)?base_url('uploads/settings/').$record->newarrival_image:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="newarrival_image" name="newarrival_image">
                      <input type="text" id="newarrival_image" name="newarrival_image" value="<?php echo !empty($record->newarrival_image)?$record->newarrival_image:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('newarrival_image'); ?>
              </div>   

              
               <div class="form-group">
                  <label>New Product Name</label>
                  <input type="name" class="form-control" id="newarrival_name" name="newarrival_name" value="<?php echo !empty($record->newarrival_name)?$record->newarrival_name:''?>" required>
                  <?php echo form_error('newarrival_name'); ?>
                </div> 
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>  
              </div>  
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>