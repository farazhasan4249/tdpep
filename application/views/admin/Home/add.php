<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Slider</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/home/add')?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">
                <div class="form-group">
                  <label>Slider Heading:</label>
                  <input type="text" class="form-control" id="home_slider_heading" name="home_slider_heading" required>
                  <?php echo form_error('home_slider_heading'); ?>
                </div>
                
                
                <div class="form-group">
                    <label>Slider Image</label>
                    <div class="input-group-btn">
                      <div class="image-upload">                      
                        <img src="<?php echo base_url('assets/admin/img/placeholder.png')?>">
                        <div class="file-btn">
                          <input type="file" id="home_slider_image" name="home_slider_image">
                          <label class="btn btn-info">Upload</label>
                        </div>
                      </div>
                    </div>
                    <?php echo form_error('home_slider_image'); ?>
                </div>

                 <div class="form-group">
                  <label>Slider Text</label>
                  <textarea class="editor form-control" rows="3" id="home_slider_text" name="home_slider_text" required><?php echo !empty($record->home_slider_text)?$record->home_slider_text:''?></textarea>
                  <?php echo form_error('home_slider_text'); ?>
                </div>


              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>