<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">View slider</h3>
          </div>     
          <div class="col-md-6">
            <div class="box-body">
              <div class="form-group">
                  <span class="col-md-2 view_label">Slider Name</span>
                  <span class="col-md-10 view_details"><?php echo $record->home_slider_heading;?></span>
              </div>
            
              
              <div class="form-group">
                  <span class="col-md-2 view_label">Slider Image</span>
                  <span class="col-md-10 view_details"><img style="height:150px" src="<?php echo base_url('uploads/settings/').$record->home_slider_image;?>"></span>
              </div>

              <div class="form-group">
                  <span class="col-md-2 view_label">Slider Text</span>
                  <span class="col-md-10 view_details"><?php echo $record->home_slider_text;?></span>
              </div>

            </div>      
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>
