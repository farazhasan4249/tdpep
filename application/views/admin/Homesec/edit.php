<div class="content-wrapper">
  <section class="content-header">
    <h1>
        <?php echo !empty($title)?$title:'Title';?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data</h3>
          </div>     
          <div class="col-md-6">
            <form role="form" action="<?php echo base_url('admin/Homesec/edit');?>" method="post" enctype="multipart/form-data">       
              <div class="box-body">              
                <div class="form-group">
                  <label>Home Sec Heading</label>
                  <input type="name" class="form-control" id="home_section_heading" name="home_section_heading" value="<?php echo !empty($record->home_section_heading)?$record->home_section_heading:''?>" required>
                  <?php echo form_error('home_section_heading'); ?>
                </div>              
                <div class="form-group">
                  <label>Home Sec Content Heading</label>
                  <input type="name" class="form-control" id="home_section_content_heading" name="home_section_content_heading" value="<?php echo !empty($record->home_section_content_heading)?$record->home_section_content_heading:''?>" required>
                  <?php echo form_error('home_section_content_heading'); ?>
                </div>
      
                <div class="form-group">
                  <label>Home Sec Text</label>
                  <textarea class="editor form-control" rows="3" id="home_section_text" name="home_section_text" required><?php echo !empty($record->home_section_text)?$record->home_section_text:''?></textarea>
                  <?php echo form_error('home_section_text'); ?>
                </div>
              </div>
                <div class="form-group">
                <label>Video</label>
                <div class="input-group-btn">
                  <div class="image-upload">                      
                    <img src="<?php echo !empty($record->home_section_video)?base_url('uploads/videos/').$record->home_section_video:base_url('assets/admin/img/placeholder.png')?>">
                    <div class="file-btn">
                      <input type="file" id="home_section_video" name="home_section_video">
                      <input type="text" id="home_section_video" name="home_section_video" value="<?php echo !empty($record->home_section_video)?$record->home_section_video:''?>" hidden>
                      <label class="btn btn-info">Upload</label>
                    </div>
                  </div>
                </div>
                <?php echo form_error('home_section_video'); ?>
              </div>  

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>    
            </form>        
          </div>
        </div>   
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".video-upload").on("change","input[type=file]",function() {      
            $(this).parents('.pro_videos').children('.col-md-4').children('.video-up').html('<img class="preview" src="' + URL.createObjectURL(this.files[0]) + '" /><i class="fa fa-times" aria-hidden="true"></i>');
          });
      
         $(".video-upload").on("click",".video-up i.fa.fa-times",function() {  
            $(this).parents('.video-up').html('');
            $(this).parents('.pro_videos').children('input[type=file]').val('');
   
   });
  });
</script>