<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();	          	
	 	$this->header_logo = $this->general->get_single_field('settings','','settings_logo');	
	 	$this->site_title = $this->general->get_single_field('settings','','settings_site_title');	
	 	$this->phone = $this->general->get_single_field('settings','','settings_phone');
	 	$this->address = $this->general->get_single_field('settings','','settings_address');	
	 	$this->email_address = $this->general->get_single_field('settings','','settings_email');		
	 	$this->email_from = $this->general->get_single_field('settings','','settings_email_from');			
	 	$this->email_to = $this->general->get_single_field('settings','','settings_email_to');	
        $this->favicon = $this->general->get_single_field('settings','','settings_favicon');
	 	//$this->footer_txt = $this->general->get_single_field('settings','','footer_txt');			
        $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
      /*  $data['table'] = 'social'; 
        $data['output_type'] = 'result'; 
        $this->links = $this->general->get($data);*/
	}


}
