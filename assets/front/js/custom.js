


$('.sub-menu ul').hide();
$(".sub-menu a").click(function () {
  $(this).parent(".sub-menu").children("ul").slideToggle("100");
  $(this).find(".right").toggleClass("fa-caret-up fa-caret-down");
});




/**** TIMER 1 ****/

// var deadline = new Date("july 30, 2020 15:37:25").getTime();             
// var x = setInterval(function() {
//  var currentTime = new Date().getTime();                
//  var t = deadline - currentTime; 
//  var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
//  var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
//  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
//  var seconds = Math.floor((t % (1000 * 60)) / 1000); 
//  document.getElementById("day").innerHTML =days ; 
//  document.getElementById("hour").innerHTML =hours; 
//  document.getElementById("minute").innerHTML = minutes; 
//  document.getElementById("second").innerHTML =seconds; 
//  if (t < 0) {
//   clearInterval(x); 
//   document.getElementById("time-up").innerHTML = "TIME UP"; 
//   document.getElementById("day").innerHTML ='0'; 
//   document.getElementById("hour").innerHTML ='0'; 
//   document.getElementById("minute").innerHTML ='0' ; 
//   document.getElementById("second").innerHTML = '0'; 
// } 
// }, 1000);  



/**** TIMER 1 by class ****/

var deadline = new Date("july 30, 2020 15:37:25").getTime();             
var x = setInterval(function() {
 var currentTime = new Date().getTime();                
 var t = deadline - currentTime; 
 var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
 var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
 var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
 var seconds = Math.floor((t % (1000 * 60)) / 1000); 
 $(".day").html(days) ; 
 $(".hour").html(hours); 
 $(".minute").html(minutes); 
 $(".second").html(seconds);
 if (t < 0) {
  clearInterval(x); 
  $(".day").html('0'); 
  $(".hour").html('0'); 
  $(".minute").html('0'); 
  $(".second").html('0');  
} 
}, 1000);  


// /**** TIMER 2 ****/

// var deadlineS = new Date("july 30, 2021 15:37:25").getTime();             
// var x = setInterval(function() {
//  var currentTime = new Date().getTime();                
//  var t = deadlineS - currentTime; 
//  var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
//  var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
//  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
//  var seconds = Math.floor((t % (1000 * 60)) / 1000); 
//  document.getElementById("dayS").innerHTML =days ; 
//  document.getElementById("hourS").innerHTML =hours; 
//  document.getElementById("minuteS").innerHTML = minutes; 
//  document.getElementById("secondS").innerHTML =seconds; 
//  if (t < 0) {
//   clearInterval(x); 
//   document.getElementById("time-up").innerHTML = "TIME UP"; 
//   document.getElementById("dayS").innerHTML ='0'; 
//   document.getElementById("hourS").innerHTML ='0'; 
//   document.getElementById("minuteS").innerHTML ='0' ; 
//   document.getElementById("secondS").innerHTML = '0'; 
// } 
// }, 1000);  


// /**** TIMER 3 ****/

// var deadlineT = new Date("july 30, 2022 15:37:25").getTime();             
// var x = setInterval(function() {
//  var currentTime = new Date().getTime();                
//  var t = deadlineT - currentTime; 
//  var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
//  var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
//  var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
//  var seconds = Math.floor((t % (1000 * 60)) / 1000); 
//  document.getElementById("dayT").innerHTML =days ; 
//  document.getElementById("hourT").innerHTML =hours; 
//  document.getElementById("minuteT").innerHTML = minutes; 
//  document.getElementById("secondT").innerHTML =seconds; 
//  if (t < 0) {
//   clearInterval(x); 
//   document.getElementById("time-up").innerHTML = "TIME UP"; 
//   document.getElementById("dayT").innerHTML ='0'; 
//   document.getElementById("hourT").innerHTML ='0'; 
//   document.getElementById("minuteT").innerHTML ='0' ; 
//   document.getElementById("secondT").innerHTML = '0'; 
// } 
// }, 1000);  






/**** TOP BUTTON *****/

var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});



/*!
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 * 
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

 /*jshint browser: true, strict: true, undef: true */
 /*global define: false */

 ( function( window ) {

  'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );



$('.slider .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


$('.toogle').click(function(){
$('.navig').slideToggle();
});


$(document).ready(function() {
  $('.dropdown-btn ').click(function() {
    $('.dropdown-container').slideUp();
    $(this).next().slideDown();
  });
});
